
/*
 * delay.s
 *
 * Created: 24-2-2017 10:35:37
 *  Author: Robojan
 */ 

	.section .text.delay_cycles,"ax",@progbits
	.align  1
	.global	delay_cycles
	.type	delay_cycles, @function
delay_cycles:
	sub		r12, 4
	asr		r12, 2
	retle	r12
delay_cycles.loop:
	sub		r12, 1
	nop
	brne	delay_cycles.loop
	ret		r12
	.size	delay_cycles, .-delay_cycles
