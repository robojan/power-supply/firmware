
/* Scheduler header files. */
#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <PM/pm.h>
#include <FLASHC/flashc.h>
#include <PDCA/pdca.h>

#include "tasks.h"

static void initClock(){
	/* Start the crystal oscillator 1 and switch the main clock to it. */
	/* Also start USB clock */ 
	pm_enable_osc1_ext_clock(&AVR32_PM);
	pm_enable_clk1(&AVR32_PM, 1);
	pm_pll_setup(&AVR32_PM, 0, 9, 1, 1, 16);
	pm_pll_set_option(&AVR32_PM, 0, 1, 1, 0);
	pm_pll_enable(&AVR32_PM, 0);
	pm_pll_setup(&AVR32_PM, 1, 7, 1, 1, 16);
	pm_pll_set_option(&AVR32_PM, 0, 1, 1, 0);
	pm_pll_enable(&AVR32_PM, 1);
	pm_wait_for_pll0_locked(&AVR32_PM);
	pm_wait_for_pll1_locked(&AVR32_PM);
	pm_cksel(&AVR32_PM, 0, 0, 0, 0, 0, 0);
	pm_gc_setup(&AVR32_PM, 3, 1, 1, 0, 0);
	flashc_set_bus_freq(FCPU);
	pm_switch_to_clock(&AVR32_PM, AVR32_PM_MCSEL_PLL0);
}

/*-----------------------------------------------------------*/

int main( void )
{
	initClock();
		
	portDBG_TRACE("Starting FreeRTOS");
	portDBG_TRACE("Power supply controller")
	
	// Create tasks
	createGraphicsTask();
	createUITask();

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle
	task. */

	return 0;
}
/*-----------------------------------------------------------*/
