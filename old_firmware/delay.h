/*
 * delay.h
 *
 * Created: 18-2-2017 23:20:47
 *  Author: Robojan
 */ 


#ifndef DELAY_H_
#define DELAY_H_

#define delay_ns(n) delay_cycles(((n)*FCPU)/1000000000UL)

void delay_cycles(uint32_t cycles);
	

#endif /* DELAY_H_ */