/*
 * board.h
 *
 * Created: 18-2-2017 14:07:44
 *  Author: Robojan
 */ 


#ifndef BOARD_H_
#define BOARD_H_

// Pin definitions
// LCD GPIO connections
// WARNING !!!! - when changing these assignments also change the functions
// lcd_setDatabus and lcd_getDatabus in graphicsTask.c
#define LCD_D0               AVR32_PIN_PA31
#define LCD_D1               AVR32_PIN_PA30
#define LCD_D2               AVR32_PIN_PA08
#define LCD_D3               AVR32_PIN_PA07
#define LCD_D4               AVR32_PIN_PA06
#define LCD_D5               AVR32_PIN_PA05
#define LCD_D6               AVR32_PIN_PA04
#define LCD_D7               AVR32_PIN_PA03
#define LCD_D8               AVR32_PIN_PA27
#define LCD_D9               AVR32_PIN_PA26
#define LCD_D10              AVR32_PIN_PA25
#define LCD_D11              AVR32_PIN_PA24
#define LCD_D12              AVR32_PIN_PB11
#define LCD_D13              AVR32_PIN_PB10
#define LCD_D14              AVR32_PIN_PB09
#define LCD_D15              AVR32_PIN_PB08
#define LCD_RST              AVR32_PIN_PB02
#define LCD_RD               AVR32_PIN_PB03
#define LCD_WR               AVR32_PIN_PB04
#define LCD_RS               AVR32_PIN_PB05
#define LCD_CS               AVR32_PIN_PB06
				             
// Touchscreen	             
#define TS_RXD               AVR32_USART2_RXD_0_1_PIN
#define TS_TXD               AVR32_USART2_TXD_0_1_PIN
#define TS_TXD_FUNC          AVR32_USART2_TXD_0_1_FUNCTION
#define TS_RXD_FUNC          AVR32_USART2_RXD_0_1_FUNCTION
#define TS_USART             AVR32_USART2
#define TS_IRQ               AVR32_USART2_IRQ
#define TS_DMA_PID_RX        AVR32_PDCA_PID_USART2_RX
#define TS_DMA_PID_TX        AVR32_PDCA_PID_USART2_TX

// Button inputs
#define SW_CLK               AVR32_PIN_PB01
#define SW_LATCH             AVR32_PIN_PA11
#define SW_DATA              AVR32_PIN_PA12
// Button mapping
#define BUTTON_F1            3
#define BUTTON_F2            6
#define BUTTON_F3            5
#define BUTTON_F4            4
#define BUTTON_L             1
#define BUTTON_R             2
#define BUTTON_C             0

// DMA channels
#define DMA_CHANNEL_TS       2

#endif /* BOARD_H_ */