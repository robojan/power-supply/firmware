/*
 * userInputTask.c
 *
 * Created: 18-2-2017 22:20:10
 *  Author: Robojan
 */

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>

#include "board.h"
#include "tasks.h"
#include <GPIO/gpio.h>
#include <USART/usart.h>
#include <INTC/intc.h>
#include <PDCA/pdca.h>
#include "delay.h"
 
static TaskHandle_t g_taskHandle;

#define TS_RX_BUFFER_SIZE 256

#define SET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrs = 1<<((x)%32)
#define RESET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrc = 1<<((x)%32)
#define GET_PIN(x) ((AVR32_GPIO.port[(x) / 32].pvr >> ((x)%32)) & 1)

static uint8_t g_ts_rx_buffer[TS_RX_BUFFER_SIZE];
static uint8_t g_ts_rx_pos = 0;
static int8_t g_ts_rx_bytesRemaining = 0;
static Bool g_ts_touchMessage = 0;

static __attribute__((__interrupt__)) void ts_irq()
{
	if(TS_USART.IMR.rxrdy) {
		uint8_t val = TS_USART.rhr & 0xFF;
		if(g_ts_rx_pos == 0) {
			if(val != 0x55 && (val & (1<<8)) != 0) {
				g_ts_rx_buffer[g_ts_rx_pos] = val;
				g_ts_touchMessage = 1;
				g_ts_rx_bytesRemaining = 3; // start counting after 2 bytes
				g_ts_rx_pos = 1;
			} else if(val == 0x55) {
				g_ts_rx_buffer[g_ts_rx_pos] = val;
				g_ts_touchMessage = 0;
				g_ts_rx_pos = 1;
			}
		} else if (g_ts_rx_pos == 1) {
			if(!g_ts_touchMessage) {
				g_ts_rx_bytesRemaining = val;
			}
			g_ts_rx_buffer[g_ts_rx_pos] = val;
			g_ts_rx_pos = 2;
		} else {
			g_ts_rx_buffer[g_ts_rx_pos] = val;
			g_ts_rx_pos++;
			g_ts_rx_bytesRemaining--;
			if(g_ts_rx_bytesRemaining <= 0) {
				
			}
		}
	}
}

static void setupPinMapping()
{
	gpio_enable_module_pin(TS_RXD, TS_RXD_FUNC);
	gpio_enable_module_pin(TS_TXD, TS_TXD_FUNC);
	gpio_enable_gpio_pin(SW_CLK);
	gpio_enable_gpio_pin(SW_LATCH);
	gpio_enable_gpio_pin(SW_DATA);
	AVR32_GPIO.port[SW_CLK/32].oders = 1 << (SW_CLK % 32);
	AVR32_GPIO.port[SW_LATCH/32].oders = 1 << (SW_LATCH % 32);
	AVR32_GPIO.port[SW_DATA/32].oderc = 1 << (SW_DATA % 32);
	RESET_PIN(SW_CLK);
	SET_PIN(SW_LATCH);
}

static void initTouchScreen()
{	
	const usart_options_t usart_options = {
		.baudrate = 9600,
		.charlength = 8,
		.paritytype = USART_NO_PARITY,
		.stopbits = 1,
		.channelmode = USART_NORMAL_CHMODE,
	};
	usart_init_rs232(&TS_USART, &usart_options, FPBA);
	
	TS_USART.IER.rxrdy = 1;
		
	INTC_register_interrupt(ts_irq, TS_IRQ, 0);
	
}

static uint8_t getButtonState() 
{
	uint8_t data = 0;
	RESET_PIN(SW_LATCH);
	delay_ns(200);
	SET_PIN(SW_LATCH);
	delay_ns(200);
	for(int i = 0; i < 8; i++)
	{
		delay_ns(200);
		SET_PIN(SW_CLK);
		delay_ns(70);
		data = (data << 1) | GET_PIN(SW_DATA);
		delay_ns(70);
		RESET_PIN(SW_CLK);
	}
	return data;
}


void createUITask()
{
	BaseType_t xReturned;
	 
	xReturned = xTaskCreate(uiTask, "User Input", 256, NULL,
	TASK_PRIORITY_USERINPUT, &g_taskHandle);

	configASSERT(xReturned == pdPASS);
}

 
void uiTask(void *parameters)
{
	(void) parameters;
	setupPinMapping();
	initTouchScreen();
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		// Retrieve button state
		uint8_t buttons = getButtonState();
		printf("Buttons: %02x\n", buttons);
		//if(buttons ^ g_buttonState)
		
		vTaskDelayUntil(&xLastWakeTime, /*TASK_PERIOD_USERINPUT*/ 1000);
	}
	 
	vTaskDelete(NULL);
}