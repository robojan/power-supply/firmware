/*
 * log.c
 *
 * Created: 18-2-2017 01:35:01
 *  Author: Robojan
 */ 

#include <stdio.h>
#include <stdarg.h>
#include <FreeRTOS.h>
#include <task.h>

void logMsg(const char *format, ...)
{
	va_list args; 
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
}

void log_fatal(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	TaskHandle_t handle = xTaskGetCurrentTaskHandle();
	while(1==1) 
	{
		vTaskSuspend(handle);
	}
}

void log_assert(Bool cond, const char *msg)
{
	if(!cond) {
		log_fatal(msg);
	}
}