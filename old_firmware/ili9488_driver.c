/*
 * ili9488_driver.c
 *
 * Created: 18-2-2017 02:30:38
 *  Author: Robojan
 */ 

#include "ili9488_driver.h"

typedef int lcd_api_t;

static lcd_api_t g_api;

static void lcd_getSize(lcd_api_t *api, emgl_coord_t *width, emgl_coord_t *height)
{
	(void) api;
	*width = 480;
	*height = 320;
}

static void lcd_poll(lcd_api_t *api)
{
	(void) api;
}

static void lcd_init(lcd_api_t *api)
{
	(void) api;
}

static const emgl_driverAPI_t g_driver = {
	.api = (void*)&g_api,
	.poll = (void(*)(void*))lcd_poll,
	.getPixel = NULL,
	.setPixel = NULL,
	.fillRect = NULL,
	.drawLineH = NULL,
	.drawLineV = NULL,
	.drawBitmap = NULL,
	.init = (void (*)(void*))lcd_init,
	.getSize = (void (*)(void*, emgl_coord_t *, emgl_coord_t *))lcd_getSize,
};
 
const emgl_driverAPI_t *lcd_getDriver() {
	return &g_driver;
}

