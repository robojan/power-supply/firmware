/*
 * log.h
 *
 * Created: 18-2-2017 01:35:14
 *  Author: Robojan
 */ 


#ifndef LOG_H_
#define LOG_H_

void logMsg(const char *format, ...);
void log_fatal(const char *format, ...);
void log_assert(Bool cond, const char *msg);



#endif /* LOG_H_ */