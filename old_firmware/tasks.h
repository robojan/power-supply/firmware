/*
 * tasks.h
 *
 * Created: 18-2-2017 00:24:31
 *  Author: Robojan
 */ 


#ifndef TASKS_H_
#define TASKS_H_

// Task priorities
#define TASK_PRIORITY_GRAPHICS   1
#define TASK_PRIORITY_USERINPUT  1
#define TASK_PRIORITY_CONTROL    5

// Task periods
#define TASK_PERIOD_USERINPUT    10

// Shared variables
extern uint8_t g_buttonState; 

// Task creation
void createGraphicsTask();
void createUITask();

// Task definitions
void graphicsTask(void *parameters);
void uiTask(void *parameters);



#endif /* TASKS_H_ */