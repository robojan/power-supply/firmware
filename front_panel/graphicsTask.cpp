/*
 * graphicsTask.c
 *
 * Created: 18-2-2017 00:23:51
 *  Author: Robojan
 */ 
 
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <AVR32/io.h>
#include <gpio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include "board.h"
#include "tasks.h"
#include "ili9488_driver.h"
#include "log.h"
#include "ili9488_interface.h"
#include "settings.h"

#include <emgl/emgl.h>
#include "fonts/Luna18.h"
#include "fonts/PTMonoBoldNumeric36.h"

#ifdef __cplusplus
}
#endif

#include <emgl/emgl_ui.h>
#include "ui/ui_channel.h"
#include "ui/StatusPanel.h"
#include "ui/CalibrationMarker.h"
#include "ui/CalibrationPanel.h"

//#define UI_COLOR emgl_colorConvFromRGBA8888(0xFF5F8938)
#define UI_COLOR emgl_colorConvFromRGBA8888(0xFFD0D000)
#define UI_FG_COLOR COLOR_BLACK
#define CALIB_COLOR COLOR_GRAY

enum UI_IDS{
	ID_BUTTON_F1,
	ID_BUTTON_F2,
	ID_BUTTON_F3,
	ID_BUTTON_F4
};

static const char * const mainButtonStrs[4] = {"CH1 /\nCH2", "Voltage /\ncurrent", "Output \nEnable", "Settings"};
static const char * const settingsButtonStrs[4] = {"Calibration", "", "", "Back"};
static const char * const calibrationStrs[4] = {"Calibrate\nTouch", "Calibrate\nCH1", "Calibrate\nCH2", "Back"};
static const char * const confirmButtonStrs[4] = {"OK", "Cancel", "", ""};
static const char * const calibCHButtonStrs[4] = {"Setting", "Reading", "Add\nPoint", "Confirm"};

const EMGL_font_t *EMGL_DEFAULT_FONT = &font_Luna18;

static emgl::Panel mainPanel(NULL, emgl::Point(0,0), emgl::Size(480, 320));
static ui::Channel channel1Panel(&mainPanel, emgl::Point(0, 40 + 140), 
	emgl::Size(380, 140), 1);
static ui::Channel channel2Panel(&mainPanel, emgl::Point(0, 40),
	emgl::Size(380, 140), 2);
static emgl::Panel buttonPanel(NULL, emgl::Point(380, 0), 
	emgl::Size(100, 320));
static ui::StatusPanel statusPanel(&mainPanel, emgl::Point(0, 0), 
	emgl::Size(380, 40));
static emgl::FlatButton button_f1(&buttonPanel, emgl::Point(3, 241),
	emgl::Size(94, 77), "", ID_BUTTON_F1);
static emgl::FlatButton button_f2(&buttonPanel, emgl::Point(3, 161),
	emgl::Size(94, 77), "", ID_BUTTON_F2);
static emgl::FlatButton button_f3(&buttonPanel, emgl::Point(3, 81),
	emgl::Size(94, 77), "", ID_BUTTON_F3);
static emgl::FlatButton button_f4(&buttonPanel, emgl::Point(3, 1), 
	emgl::Size(94, 77), "", ID_BUTTON_F4);

static emgl::Panel *tsCalibrationPanel = NULL;
static ui::CalibrationMarker *calibrationMarker = NULL;
static emgl::Text *tsCalibrationText = NULL;
static emgl::Panel *msgPanel = NULL;
static emgl::Text *msgText= NULL;
static ui::CalibrationPanel *calibrationPanel = NULL;

static UIState g_state = (UIState)-1;
static UIState g_nextState = STATE_init;
static void (*update_func)(void) = NULL;
static emgl::Element *g_topElement = NULL;
static int g_active_channel = 0;
static bool (*g_buttonFunc)(emgl::Element *sender, void *user, bool pressed) = NULL;
static int g_calib_channel = 0;

TaskHandle_t g_graphics_taskHandle = NULL;
static StaticTask_t g_graphics_task;
static StackType_t g_graphics_stack[TASK_STACKSIZE_GRAPHICS];
static StaticSemaphore_t g_graphicsState_mutex_buffer;
static SemaphoreHandle_t g_graphicsState_mutex;

static void update_main()
{
	static TickType_t lastUpdate = 0;
	TickType_t now = xTaskGetTickCount();
	if(lastUpdate - now > 100*portTICK_PERIOD_MS)
	{
		channel1Panel.update(GetCalibratedADCVoltage(0, g_dispVoltage[0]), GetCalibratedADCCurrent(0, g_dispCurrent[0]), g_cc[0]);
		channel2Panel.update(GetCalibratedADCVoltage(1, g_dispVoltage[1]), GetCalibratedADCCurrent(1, g_dispCurrent[1]), g_cc[1]);
		statusPanel.update(g_temperature, getOutputEnabled(0), getOutputEnabled(1));
		lastUpdate = now;
	}

}

static void update_calibration()
{

}

static void setActiveChannel(int channel) {
	log_assert(channel >= 0 && channel < 2, "Channel >= 0 && channel < 2");
	if(channel == g_active_channel) return;
	g_active_channel = channel;
	channel1Panel.setActive( channel == 0);
	channel2Panel.setActive( channel == 1);
	channel2Panel.Refresh();
	channel1Panel.Refresh();
}

static bool mainButtonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(!pressed) return true;
	int id = sender->GetID();
	switch(id) {
	case ID_BUTTON_F1:
		setActiveChannel( (g_active_channel + 1) & 1);
		return true;
	case ID_BUTTON_F2:
		switch(g_active_channel) {
			case 0:
			channel1Panel.switchSetting();
			break;
			case 1:
			channel2Panel.switchSetting();
			break;
		}
		return true;
	case ID_BUTTON_F3:
		setOutputEnabled(g_active_channel, !getOutputEnabled(g_active_channel));
		return true;
	case ID_BUTTON_F4:
		setNextUIState(STATE_settings);
		return true;
	default:
		return false;
	}
}

static bool confirmCalibButtonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(!pressed) return true;
	int id = sender->GetID();
	switch(id) {
	case ID_BUTTON_F1: // OK
		setNextUIState(STATE_chcal_vcal);
		return true;
	case ID_BUTTON_F2: // Cancel
		setNextUIState(STATE_calibSettings);
		return true;
	case ID_BUTTON_F3:
		return false;
	case ID_BUTTON_F4:
		return false;
	default:
		return false;
	}
}

static bool calibSettingsButtonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(!pressed) return true;
	int id = sender->GetID();
	switch(id) {
	case ID_BUTTON_F1:
		startTSCalibration();
		return true;
	case ID_BUTTON_F2:
		g_calib_channel = 0;
		setNextUIState(STATE_confirmCalib);
		return true;
	case ID_BUTTON_F3:
		g_calib_channel = 1;
		setNextUIState(STATE_confirmCalib);
		return true;
	case ID_BUTTON_F4:
		setNextUIState(STATE_settings);
		return true;
	default:
		return false;
	}
}

static bool settingsButtonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(!pressed) return true;
	int id = sender->GetID();
	switch(id) {
	case ID_BUTTON_F1:
		setNextUIState(STATE_calibSettings);
		return true;
	case ID_BUTTON_F2:
		return false;
	case ID_BUTTON_F3:
		return false;
	case ID_BUTTON_F4:
		setNextUIState(STATE_mainWindow);
		return true;
	default:
		return false;
	}
}

static bool calibButtonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(!pressed) return true;
	if(calibrationPanel == NULL) return false;
	int id = sender->GetID();
	switch(id) {
	case ID_BUTTON_F1:
		calibrationPanel->ChangeSetting();
		return true;
	case ID_BUTTON_F2:
		calibrationPanel->ChangeReadout();
		return true;
	case ID_BUTTON_F3:
		calibrationPanel->AddPoint();
		return true;
	case ID_BUTTON_F4:{
			logMsg("Voltage Calibration complete\n");
			logMsg("DAC calibration points:\n");
			std::map<int, int> &dacpoints = calibrationPanel->GetCalibrationDACPoints();
			std::map<int, int> &adcpoints = calibrationPanel->GetCalibrationADCPoints();
			int numDacPoints = dacpoints.size();
			int numAdcPoints = adcpoints.size();
			int16_t *dacSetting = new int16_t[numDacPoints];
			int16_t *dacValue = new int16_t[numDacPoints];
			int16_t *readoutValues = new int16_t[numAdcPoints];
			int i = 0;
			for(std::map<int,int>::iterator it = dacpoints.begin(); it != dacpoints.end(); it++)
			{
				dacSetting[i] = it->second;
				dacValue[i++] = it->first;
				logMsg("\t%d = %d\n", it->second, it->first);
			}
			logMsg("ADC calibration points:\n");
			i = 0;
			for(std::map<int,int>::iterator it = adcpoints.begin(); it != adcpoints.end(); it++)
			{
				readoutValues[i++] = it->first;
				logMsg("\t%d = %d\n", it->second, it->first);
			}
			if(g_state == STATE_chcal_vcal) {
				StoreVoltageCalibration(g_calib_channel, numDacPoints, dacValue, dacSetting, readoutValues);
				setNextUIState(STATE_chcal_ical);
			} else {
				StoreCurrentCalibration(g_calib_channel, numDacPoints, dacValue, dacSetting, readoutValues);
				SaveSettings();
				setNextUIState(STATE_settings);			
			}
			delete[] dacSetting;
			delete[] dacValue;
			delete[] readoutValues;
			return true;
		}
	default:
		return false;
	}
}

static bool buttonCallback(emgl::Element *sender, void *user, bool pressed)
{
	if(g_buttonFunc) {
		return g_buttonFunc(sender, user, pressed);
	}
	return false;
}

static void settingChangedCallback(int channel, enum ui::Channel::editingField field, int setting)
{
	switch(field) {
	case ui::Channel::VoltageSetting:
		setChannelPreVoltage(channel-1, setting+300);
		setChannelRawVoltage(channel-1, GetCalibratedDACVoltage(channel - 1, setting));
		break;
	case ui::Channel::CurrentSetting:
		setChannelRawCurrent(channel-1, GetCalibratedDACCurrent(channel - 1, setting));
		break;
	}
}

static emgl::Point getCalibrationPos(int id) 
{
	emgl::Size size = emgl::Size(480, 320);
	switch(id) {
		default:
		case 0:
			return emgl::Point(size.width * 1 / 8, size.height * 7 / 8);
		case 1:
			return emgl::Point(size.width * 7 / 8, size.height * 7 / 8);
		case 2:
			return emgl::Point(size.width * 7 / 8, size.height * 1 / 8);
		case 3:
			return emgl::Point(size.width * 1 / 8, size.height * 1 / 8);
	}
}

static bool isCalibrationDone() {
	return isTSCalibrated();
}

static void createTSCalibrationPanel() {
	if(tsCalibrationPanel == NULL) {
		tsCalibrationPanel = new emgl::Panel(NULL, emgl::Point(0,0), emgl::Size(480, 320));
		tsCalibrationPanel->setBackground(CALIB_COLOR);
	}
	if(calibrationMarker == NULL) {
		emgl::Point pos = getCalibrationPos(0) - emgl::Point(20,20);
		calibrationMarker = new ui::CalibrationMarker(tsCalibrationPanel, pos, emgl::Size(40,40));
	}
	if(tsCalibrationText == NULL) {
		tsCalibrationText = new emgl::Text(tsCalibrationPanel, emgl::Point(100,80), 
			emgl::Size(280,160), "Touch and release target", -1, emgl::ALIGN_CENTER, emgl::ALIGN_CENTER, true);
		tsCalibrationText->setBackground(CALIB_COLOR);
	}
	g_buttonFunc = NULL;
}

static void destroyTSCalibrationPanel() {
	if(tsCalibrationPanel) {
		delete tsCalibrationPanel;
		tsCalibrationText = NULL;
		calibrationMarker = NULL;
		tsCalibrationPanel = NULL;
	}
}

static void createCalibrationPanel(ui::CalibrationPanel::calibratationType type) {
	if(calibrationPanel == NULL) {
		calibrationPanel = new ui::CalibrationPanel(NULL, emgl::Point(0,0), emgl::Size(480,320), g_calib_channel + 1, type);
		calibrationPanel->AddChild(&buttonPanel, false);
		calibrationPanel->setBackground(UI_COLOR);
		calibrationPanel->setForeground(UI_FG_COLOR);
	}
}

static void destroyCalibrationPanel() {
	if(calibrationPanel != NULL) {
		mainPanel.AddChild(&buttonPanel, false);
		delete calibrationPanel;
		calibrationPanel = NULL;
	}
}

static void createMsgPanel(const char *msg) {
	if(msgPanel == NULL) {
		msgPanel = new emgl::Panel(NULL, emgl::Point(0,0), emgl::Size(480,320));
		msgPanel->AddChild(&buttonPanel, false);
		msgPanel->setBackground(UI_COLOR);
		msgPanel->setForeground(UI_FG_COLOR);
	}
	if(msgText == NULL) {
		msgText = new emgl::Text(msgPanel, emgl::Point(0,0), emgl::Size(380, 320), msg, -1, emgl::ALIGN_CENTER, emgl::ALIGN_CENTER);
		msgText->setBackground(UI_COLOR);
		msgText->setForeground(UI_FG_COLOR);
	}
}

static void destroyMsgPanel() {
	if(msgPanel) {
		mainPanel.AddChild(&buttonPanel, false);
		delete msgPanel;
		msgPanel = NULL;
		msgText = NULL;
	}
}

void setButtonTexts(const char * const strs[4])
{
	button_f1.setLabel(strs[0]);
	button_f2.setLabel(strs[1]);
	button_f3.setLabel(strs[2]);
	button_f4.setLabel(strs[3]);
	button_f1.Refresh();
	button_f2.Refresh();
	button_f3.Refresh();
	button_f4.Refresh();
}

void setNextUIState(enum UIState nextState) {
	xSemaphoreTake(g_graphicsState_mutex, portMAX_DELAY);
	g_nextState = nextState;
	xSemaphoreGive(g_graphicsState_mutex);
}

static void switchUIState() {
	xSemaphoreTake(g_graphicsState_mutex, portMAX_DELAY);
	switch(g_nextState) {
		default:
			xSemaphoreGive(g_graphicsState_mutex);
			portDBG_TRACE("Unknown graphics next state %d", g_nextState);
			return;
		case STATE_mainWindow:
			destroyCalibrationPanel();
			destroyTSCalibrationPanel();
			destroyMsgPanel();
			g_topElement = &mainPanel;
			update_func = update_main;			
			g_buttonFunc = mainButtonCallback;
			setButtonTexts(mainButtonStrs);
			channel1Panel.setActive(g_active_channel == 0);
			channel2Panel.setActive(g_active_channel == 1);
			refreshWindow();
			break;
		case STATE_settings:
			destroyCalibrationPanel();
			destroyTSCalibrationPanel();
			destroyMsgPanel();
			g_buttonFunc = settingsButtonCallback;
			update_func = update_main;
			g_topElement = &mainPanel;
			setButtonTexts(settingsButtonStrs);
			refreshWindow();
			break;
		case STATE_calibSettings:
			destroyCalibrationPanel();
			destroyTSCalibrationPanel();
			destroyMsgPanel();
			g_buttonFunc = calibSettingsButtonCallback;
			update_func = update_main;
			g_topElement = &mainPanel;
			setButtonTexts(calibrationStrs);
			refreshWindow();
			break;
		case STATE_confirmCalib:
			g_buttonFunc = confirmCalibButtonCallback;
			setButtonTexts(confirmButtonStrs);
			if(msgPanel == NULL) {
				createMsgPanel("Are you sure you want\nto recalibrate this device.\nThis will erase the previous\ncalibration data!");
			}
			g_topElement = msgPanel;
			update_func = NULL;
			refreshWindow();
			break;
		case STATE_cal_step1:
			setOutputEnabled(0, 0);
			setOutputEnabled(1, 0);
			destroyMsgPanel();
			if(tsCalibrationPanel == NULL) {
				createTSCalibrationPanel();
			}
			g_topElement = tsCalibrationPanel;
			update_func = update_calibration;
			g_buttonFunc = NULL;
			
			refreshWindow();
			break;
		case STATE_cal_step2:
			destroyMsgPanel();
			if(tsCalibrationPanel == NULL) {
				createTSCalibrationPanel();
			}
			calibrationMarker->SetPos(getCalibrationPos(1) - emgl::Point(20,20));
			break;
		case STATE_cal_step3:
			destroyMsgPanel();
			if(tsCalibrationPanel == NULL) {
				createTSCalibrationPanel();
			}
			calibrationMarker->SetPos(getCalibrationPos(2) - emgl::Point(20,20));
			break;
		case STATE_cal_step4:
			destroyMsgPanel();
			if(tsCalibrationPanel == NULL) {
				createTSCalibrationPanel();
			}
			calibrationMarker->SetPos(getCalibrationPos(3) - emgl::Point(20,20));
			break;
		case STATE_chcal_vcal:
			destroyMsgPanel();
			destroyTSCalibrationPanel();
			setOutputEnabled(0, 0);
			setOutputEnabled(1, 0);
			setOutputEnabled(g_calib_channel, 1);
			if(calibrationPanel != NULL) {
				destroyCalibrationPanel();
			}
			createCalibrationPanel(ui::CalibrationPanel::CAL_VOLTAGE);
			g_topElement = calibrationPanel;
			g_buttonFunc = calibButtonCallback;
			setButtonTexts(calibCHButtonStrs);
			refreshWindow();
			break;
		case STATE_chcal_ical:
			destroyMsgPanel();
			destroyTSCalibrationPanel();
			setOutputEnabled(0, 0);
			setOutputEnabled(1, 0);
			setOutputEnabled(g_calib_channel, 1);
			if(calibrationPanel != NULL) {
				destroyCalibrationPanel();
			}
			createCalibrationPanel(ui::CalibrationPanel::CAL_CURRENT);
			g_topElement = calibrationPanel;
			g_buttonFunc = calibButtonCallback;
			setButtonTexts(calibCHButtonStrs);
			refreshWindow();
			break;
			
			
	}
	g_state = g_nextState;
	xSemaphoreGive(g_graphicsState_mutex);
}

void refreshWindow()
{
	if(g_topElement) {
		g_topElement->Refresh();
	}
}

void createGraphicsTask()
{	
	portDBG_TRACE("Creating task %s", "Graphics");
	g_graphics_taskHandle = xTaskCreateStatic(graphicsTask, "Graphics", 
		TASK_STACKSIZE_GRAPHICS, NULL, TASK_PRIORITY_GRAPHICS,
		g_graphics_stack, &g_graphics_task);
}

static void handleTS(uint16_t x, uint16_t y, bool pressed)
{	
	int x_screen = x;
	int y_screen = 4095-y;
	emgl_length_t lcd_width, lcd_height;
	emgl_getScreenSize(&lcd_width, &lcd_height);
	x_screen = x_screen * lcd_width / 4096;
	y_screen = y_screen * lcd_height / 4096;
	if(g_topElement) {
		g_topElement->ProcessPressedEvent(emgl::Point(x_screen, y_screen), pressed);
	}
}

void graphicsTask(void *parameters)
{
	uint32_t notification;
	portDBG_TRACE("Task started");
	(void)parameters;
	const emgl_support_t support = {
		pvPortMalloc,
		NULL,
		vPortFree,
		logMsg,
		log_fatal,
	};
	
	lcd_io_init();

	g_graphicsState_mutex = xSemaphoreCreateMutexStatic(&g_graphicsState_mutex_buffer);
	
	emgl_init(&support);
	emgl_registerDriver(lcd_getDriver());

	emgl_clear(COLOR_BLACK);

	emgl_length_t lcd_width, lcd_height;
	emgl_getScreenSize(&lcd_width, &lcd_height);

	setBacklight(1023);

	mainPanel.setBackground(emgl_colorDoubleDarker(UI_COLOR));
	mainPanel.AddChild(&buttonPanel, false);
	buttonPanel.setBackground(UI_COLOR);
	statusPanel.setBackground(UI_COLOR);
	channel1Panel.setBackground(UI_COLOR);
	channel2Panel.setBackground(UI_COLOR);
	channel1Panel.setActive(g_active_channel == 0);
	channel2Panel.setActive(g_active_channel == 1);
	channel1Panel.setCallback(settingChangedCallback);
	channel2Panel.setCallback(settingChangedCallback);
	button_f1.setBackground(emgl_colorLighter(UI_COLOR));
	button_f2.setBackground(emgl_colorLighter(UI_COLOR));
	button_f3.setBackground(emgl_colorLighter(UI_COLOR));
	button_f4.setBackground(emgl_colorLighter(UI_COLOR));
	g_buttonFunc = NULL;
	button_f1.setOnClickCallback(buttonCallback, NULL);
	button_f2.setOnClickCallback(buttonCallback, NULL);
	button_f3.setOnClickCallback(buttonCallback, NULL);
	button_f4.setOnClickCallback(buttonCallback, NULL);
	if(isCalibrationDone()) {
		setNextUIState(STATE_mainWindow);
	} else {
		startTSCalibration();
	}
	channel1Panel.setSetting(ui::Channel::CurrentSetting, getChannelCurrent(0));
	channel1Panel.setSetting(ui::Channel::VoltageSetting, getChannelVoltage(0));
	channel2Panel.setSetting(ui::Channel::CurrentSetting, getChannelCurrent(1));
	channel2Panel.setSetting(ui::Channel::VoltageSetting, getChannelVoltage(1));
	switchUIState();

	uint8_t buttonstate = 0xFF;
	int lastQECpos = 0;
	for(;;)
	{
		xSemaphoreTake(g_graphicsState_mutex, portMAX_DELAY);
		if(g_nextState != g_state) {
			xSemaphoreGive(g_graphicsState_mutex);
			switchUIState();
			xSemaphoreTake(g_graphicsState_mutex, portMAX_DELAY);
		}
		xSemaphoreGive(g_graphicsState_mutex);
		if(xTaskNotifyWait(0, TASK_NOTIFY_GRAPHICS_QEC | 
			TASK_NOTIFY_GRAPHICS_BUTTONS | TASK_NOTIFY_GRAPHICS_TOUCH, 
			&notification, TASK_TIMEOUT_GRAPHICS * portTICK_PERIOD_MS) 
			== pdTRUE) {
			// Check for notifications
			if((notification & TASK_NOTIFY_GRAPHICS_BUTTONS) != 0) {
				uint8_t newState = g_buttonState;
				uint8_t diff = buttonstate ^ newState;
				if(g_state != STATE_cal_step1 && g_state != STATE_cal_step2 &&
					g_state != STATE_cal_step3 && g_state != STATE_cal_step4) {
					if((diff & (1<<BUTTON_F1)) != 0)
						button_f1.SetPressed((newState &  (1<<BUTTON_F1)) == 0, true);
					if((diff & (1<<BUTTON_F2)) != 0)
						button_f2.SetPressed((newState &  (1<<BUTTON_F2)) == 0, true);
					if((diff & (1<<BUTTON_F3)) != 0)
						button_f3.SetPressed((newState &  (1<<BUTTON_F3)) == 0, true);
					if((diff & (1<<BUTTON_F4)) != 0)
						button_f4.SetPressed((newState &  (1<<BUTTON_F4)) == 0, true);
					
				}
				if(g_topElement == &mainPanel)
				{
					if((diff & (1<<BUTTON_L)) != 0 && (newState &  (1<<BUTTON_L)) == 0) {
						switch(g_active_channel) {
						case 0:
							channel1Panel.PrevSettingDigit();
							break;
						case 1:
							channel2Panel.PrevSettingDigit();
							break;
						}
					}
					if((diff & (1<<BUTTON_R)) != 0 && (newState &  (1<<BUTTON_R)) == 0) {
						switch(g_active_channel) {
						case 0:
							channel1Panel.NextSettingDigit();
							break;
						case 1:
							channel2Panel.NextSettingDigit();
							break;
						}					
					}
					if((diff & (1<<BUTTON_C)) != 0 && (newState &  (1<<BUTTON_C)) == 0) {
						switch(g_active_channel) {
						case 0:
							channel1Panel.NextSettingDigit();
							break;
						case 1:
							channel2Panel.NextSettingDigit();
							break;
						}
					}
				}
				else if(g_topElement == calibrationPanel && calibrationPanel != NULL) {
					if((diff & (1<<BUTTON_L)) != 0 && (newState &  (1<<BUTTON_L)) == 0) {
						calibrationPanel->PrevDigit();
					}
					if((diff & (1<<BUTTON_R)) != 0 && (newState &  (1<<BUTTON_R)) == 0) {
						calibrationPanel->NextDigit();
					}
					if((diff & (1<<BUTTON_C)) != 0 && (newState &  (1<<BUTTON_C)) == 0) {
						calibrationPanel->NextDigit();
					}
				}
					
				buttonstate = newState;
			}
			if((notification & TASK_NOTIFY_GRAPHICS_TOUCH) != 0) {
				uint16_t x, y;
				bool pressed;
				xSemaphoreTake(g_ts_val_semaphore, portMAX_DELAY);
				x = g_ts_x;
				y = g_ts_y;
				pressed = g_ts_pressed != 0;
				xSemaphoreGive(g_ts_val_semaphore);
				handleTS(x, y, pressed);
			}
			if((notification & TASK_NOTIFY_GRAPHICS_QEC) != 0) {
				if(g_topElement == &mainPanel)
				{
					switch(g_active_channel)
					{
						case 0:
						channel1Panel.modifyDigit(g_qec_pos - lastQECpos);
						break;
						case 1:
						channel2Panel.modifyDigit(g_qec_pos - lastQECpos);
						break;
					}
				}if(g_topElement == calibrationPanel && calibrationPanel != NULL)
				{
					calibrationPanel->ModifyDigit(g_qec_pos - lastQECpos);
				}
				lastQECpos = g_qec_pos;		
			}
		} 
		if(update_func) {
			update_func();
		}
	}
	
	vTaskDelete(NULL);
}