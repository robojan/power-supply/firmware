/*
 * ili9488_interface.h
 *
 * Created: 2-4-2017 17:53:04
 *  Author: Robojan
 */ 


#ifndef ILI9488_INTERFACE_H_
#define ILI9488_INTERFACE_H_


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <emgl/emgl.h>

void lcd_setDatabusInput();
void lcd_setDatabusOutput();
void lcd_setDatabus(uint16_t data);
uint16_t lcd_getDatabus();
void lcd_io_init();
void lcd_writeCmdStart(emgl_U16 cmd);
void lcd_writeCmdEnd();
void lcd_writeData(emgl_U16 data);
void lcd_fillWrite(emgl_U16 cmd, emgl_U16 data, emgl_U32 n);
void lcd_writeCmd(emgl_U16 cmd, emgl_U32 n, const emgl_U16 *data);
void lcd_readCmd(emgl_U16 cmd, emgl_U32 n, emgl_U16 *data);
void lcd_reset();


#ifdef __cplusplus
}
#endif

#endif /* ILI9488_INTERFACE_H_ */