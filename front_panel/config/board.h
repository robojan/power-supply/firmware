/**
 * \file
 *
 * \brief User board definition template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

/* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */

#define FOSC1 12000000UL
#define FCPU 60000000UL
#define FHSB FCPU
#define FPBA 60000000UL
#define FPBB 60000000UL

// Pin definitions
// LCD GPIO connections
// WARNING !!!! - when changing these assignments also change the functions
// lcd_setDatabus and lcd_getDatabus in graphicsTask.c
#define LCD_D0               AVR32_PIN_PA31
#define LCD_D1               AVR32_PIN_PA30
#define LCD_D2               AVR32_PIN_PA08
#define LCD_D3               AVR32_PIN_PA07
#define LCD_D4               AVR32_PIN_PA06
#define LCD_D5               AVR32_PIN_PA05
#define LCD_D6               AVR32_PIN_PA04
#define LCD_D7               AVR32_PIN_PA03
#define LCD_D8               AVR32_PIN_PA27
#define LCD_D9               AVR32_PIN_PA26
#define LCD_D10              AVR32_PIN_PA25
#define LCD_D11              AVR32_PIN_PA24
#define LCD_D12              AVR32_PIN_PB11
#define LCD_D13              AVR32_PIN_PB10
#define LCD_D14              AVR32_PIN_PB09
#define LCD_D15              AVR32_PIN_PB08
#define LCD_RST              AVR32_PIN_PB02
#define LCD_RD               AVR32_PIN_PB03
#define LCD_WR               AVR32_PIN_PB04
#define LCD_RS               AVR32_PIN_PB05
#define LCD_CS               AVR32_PIN_PB06

// Touchscreen
#define TS_RXD               AVR32_USART2_RXD_0_1_PIN
#define TS_TXD               AVR32_USART2_TXD_0_1_PIN
#define TS_TXD_FUNC          AVR32_USART2_TXD_0_1_FUNCTION
#define TS_RXD_FUNC          AVR32_USART2_RXD_0_1_FUNCTION
#define TS_USART             AVR32_USART2
#define TS_IRQ               AVR32_USART2_IRQ
#define TS_DMA_PID_RX        AVR32_PDCA_PID_USART2_RX
#define TS_DMA_PID_TX        AVR32_PDCA_PID_USART2_TX

// Button inputs
#define SW_CLK               AVR32_PIN_PB01
#define SW_LATCH             AVR32_PIN_PA11
#define SW_DATA              AVR32_PIN_PA12

// Button mapping
#define BUTTON_F1            4
#define BUTTON_F2            7
#define BUTTON_F3            6
#define BUTTON_F4            5
#define BUTTON_L             3
#define BUTTON_R             2
#define BUTTON_C             1

// Quadrature Encoder
#define QEC_A                AVR32_EIC_EXTINT_2_PIN
#define QEC_A_FUNC           AVR32_EIC_EXTINT_2_FUNCTION
#define QEC_A_IRQ            AVR32_EIC_IRQ_2
#define QEC_A_EIC_INT        AVR32_EIC_INT2
#define QEC_B                AVR32_PIN_PA13
#define QEC_PC_IRQ           AVR32_GPIO_IRQ_1
#define QEC_MASK             ((1<<QEC_A)|(1<<QEC_B))
#define QEC_OFFSET           QEC_B

// Misc
#define BL                   AVR32_PWM_6_0_PIN
#define BL_FUNC              AVR32_PWM_6_0_FUNCTION
#define BL_PWM               AVR32_PWM_CHID6
#define BL_MASK              AVR32_PWM_CHID6_MASK
#define FAN                  AVR32_PWM_4_2_PIN
#define FAN_FUNC             AVR32_PWM_4_2_FUNCTION
#define FAN_PWM              AVR32_PWM_CHID4
#define FAN_MASK             AVR32_PWM_CHID4_MASK

// i2c
#define I2C                  AVR32_TWI
#define I2C_SDA              AVR32_TWI_SDA_0_0_PIN
#define I2C_SDA_FUNC         AVR32_TWI_SDA_0_0_FUNCTION
#define I2C_SCL              AVR32_TWI_SCL_0_0_PIN
#define I2C_SCL_FUNC         AVR32_TWI_SCL_0_0_FUNCTION

#define I2C_CHA_PREPOT_ADDR  0x2E
#define I2C_CHA_ADC_ADDR     0x49
#define I2C_CHA_DACI_ADDR    0x0D
#define I2C_CHA_DACV_ADDR    0x0C
#define I2C_CHB_PREPOT_ADDR  0x2F
//#define I2C_CHB_ADC_ADDR     0x48
#define I2C_CHB_ADC_ADDR     0x4B
#define I2C_CHB_DACI_ADDR    0x08
//#define I2C_CHB_DACV_ADDR    0x0E
#define I2C_CHB_DACV_ADDR    0x4E // Bug on pcb

// Temperature
#define TEMP                 AVR32_TC_CLK0_0_0_PIN
#define TEMP_FUNC            AVR32_TC_CLK0_0_0_FUNCTION
#define TEMP_TIMER           1
#define TEMP_CLK             0

// Output enables
#define OUTPUT_ENABLE_CHA    AVR32_PIN_PA15
#define OUTPUT_ENABLE_CHB    AVR32_PIN_PA17

// DMA channels

#endif // USER_BOARD_H
