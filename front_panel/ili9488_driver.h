/*
 * ili9488_driver.h
 *
 * Created: 18-2-2017 02:33:06
 *  Author: Robojan
 */ 


#ifndef ILI9488_DRIVER_H_
#define ILI9488_DRIVER_H_

#include <emgl/emgl.h>

const emgl_driverAPI_t *lcd_getDriver();

extern void lcd_writeCmd(emgl_U16 cmd, emgl_U32 n, const emgl_U16 *data);
extern void lcd_writeCmdStart(emgl_U16 cmd);
extern void lcd_writeCmdEnd();
extern void lcd_writeData(emgl_U16 data);
extern void lcd_readCmd(emgl_U16 cmd, emgl_U32 n, emgl_U16 *data);
extern void lcd_reset();


#endif /* ILI9488_DRIVER_H_ */