/*
 * EditableValue.cpp
 *
 * Created: 02-09-17 17:35:32
 *  Author: Robojan
 */ 
#include "EditableValue.h"
#include "../utils/log.h"
#include <emgl/debug.h>
#include "../utils/stringhelper.h"
#include <cstring>

using namespace ui;

EditableValue::EditableValue(emgl::Element *parent, emgl::Point pos,
emgl::Size size, int defaultValue, int digits, const char *unit /*= ""*/,
int decimalPlace /*= -1*/, int id /*= -1*/,
emgl::Alignment hAlign /*= ALIGN_LEFT*/,
emgl::Alignment vAlign /*= ALIGN_BOTTOM*/):
emgl::Text(parent, pos, size, "", id, hAlign, vAlign, false), _active(false),
_decimalPlace(decimalPlace), _editingPos(0), _nDigits(digits), _user(NULL),
_callback(NULL), _min(0), _max(9)
{
	EMGL_ASSERT(decimalPlace < digits, "decimalPlace < digits");
	int len = digits + strlen(unit) + 1;
	if(decimalPlace >= 0) len++;
	if(_text) {
		delete _text;
	}
	_text = new char[len];
	_text[len-1] = '\0';
	if(decimalPlace > 0){
		strcpy(_text + digits+1, unit);
		} else {
		strcpy(_text + digits, unit);
	}
	setValue(defaultValue);
	SetSize(Text::GetSize() + emgl::Size(0,5));
}

EditableValue::~EditableValue()
{

}

void EditableValue::setMin(int min)
{
	_min = min;
	setValue(getValue());
}

void EditableValue::setMax(int max)
{
	_max = max;
	setValue(getValue());
}

int EditableValue::getValue() const
{
	int value = 0;
	int mult = 1;
	if(_decimalPlace > 0){
		for(int i = _nDigits; i >= 0; i--){
			if(i != _decimalPlace) {
				value += (_text[i] - '0') * mult;
				mult *= 10;
			}
		}
		} else {
		for(int i = _nDigits - 1; i >= 0; i--)
		{
			value += (_text[i] - '0') * mult;
			mult *= 10;
		}
	}
	return value;
}

void EditableValue::setValue(int value)
{
	if(value < _min) value = _min;
	if(value > _max) value = _max;
	if(_decimalPlace > 0){
		for(int i = _nDigits; i >= 0; i--){
			if(i == _decimalPlace) {
				_text[i] = '.';
				} else {
				_text[i] = (value % 10) + '0';
				value /= 10;
			}
		}
		} else {
		for(int i = _nDigits - 1; i >= 0; i--)
		{
			_text[i] = (value % 10) + '0';
			value /= 10;
		}
	}
	calculateLayout();
}

void EditableValue::Draw(emgl::PaintContext &PC, emgl::Rectangle &area)
{
	//PC.offsetOrigin(GetPos());
	PC.offsetOrigin(emgl::Point(0, 5));
	Text::Draw(PC, area);
	PC.offsetOrigin(emgl::Point(0, -5));
	//PC.offsetOrigin(-GetPos());
	PC.drawFilledRectangle(emgl::Rectangle(0,0, GetSize().width, 5), getBackground());
	if(_active) {
		int idx = _editingPos;
		if(_editingPos >= _decimalPlace && _decimalPlace > 0) idx++;
		emgl::Point pos = getGlyphPos(idx);
		PC.drawFilledRectangle(emgl::Rectangle(pos.x, 0, 12, 4), getForeground());
	}
}

void EditableValue::PrevDigit()
{
	if(_editingPos > 0) {
		_editingPos--;
		if(_active) {
			Refresh();
		}
	}
}

void EditableValue::NextDigit()
{
	if(_editingPos < _nDigits - 1) {
		_editingPos++;
		if(_active) {
			Refresh();
		}
	}
}

void EditableValue::modifyDigit(int delta)
{
	if(delta == 0) {
		return;
	}
	int mult = 1;
	for(int i = _nDigits-1; _editingPos < i; i--) {
		mult *= 10;
	}
	int value = EditableValue::getValue() + delta * mult;
	
	setValue(value);
	if(_callback) _callback(this, EditableValue::getValue(), _user);
	Refresh();
}

void EditableValue::setCallback(void (*func)(EditableValue *, int, void *user), void *user)
{
	_callback = func;
	_user = user;
}

void EditableValue::setActive(bool active)
{
	_active = active;
}