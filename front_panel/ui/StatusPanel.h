/*
 * StatusPanel.h
 *
 * Created: 2-3-2017 17:34:23
 *  Author: Robojan
 */ 


#ifndef STATUSPANEL_H_
#define STATUSPANEL_H_

#include <emgl/emgl_ui.h>

namespace ui {
	class StatusPanel : public emgl::Panel
	{
	public:
		StatusPanel(emgl::Element *parent, emgl::Point pos, emgl::Size size);
		virtual ~StatusPanel();

		virtual void setBackground(emgl_color_t color);

		virtual void Draw(emgl::PaintContext &PC, emgl::Rectangle &area);

		virtual void update(int temp, bool ch1_outen, bool ch2_outen);
	protected:
		void DrawEnabledSymbol(emgl::Point pos, emgl::PaintContext &PC, emgl::Rectangle &area);
		void DrawDisabledSymbol(emgl::Point pos, emgl::PaintContext &PC, emgl::Rectangle &area);
	private:
		char _tempText[8];
		bool _outEn[2];
		emgl::Text *_tempLabel;
		emgl::Text *_output1;
		emgl::Text *_output2;
	};
}

#endif /* STATUSPANEL_H_ */