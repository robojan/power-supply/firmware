/*
 * CalibrationPanel.h
 *
 * Created: 02-09-17 17:10:24
 *  Author: Robojan
 */ 


#ifndef CALIBRATIONPANEL_H_
#define CALIBRATIONPANEL_H_


#include <emgl/emgl_ui.h>
#include <map>
#include "EditableValue.h"

namespace ui {
	class CalibrationPanel : public emgl::Panel
	{
		enum settingType{
			SET_setting,
			SET_readout,	
		};
	public:	
		enum calibratationType {
			CAL_VOLTAGE,
			CAL_CURRENT
		};
		CalibrationPanel(emgl::Element *parent, emgl::Point pos, emgl::Size size, int channel, enum calibratationType calType);
		virtual ~CalibrationPanel();
		
		virtual void setBackground(emgl_color_t color);
		virtual void setForeground(emgl_color_t color);
		
		void ChangeSetting();
		void ChangeReadout();
		void AddPoint();
		void PrevDigit();
		void NextDigit();
		void ModifyDigit(int change);
		
		std::map<int,int> &GetCalibrationDACPoints();
		std::map<int,int> &GetCalibrationADCPoints();
	private:
		static void onSettingCallback(EditableValue *sender, int value, CalibrationPanel *self);
	
		enum settingType _activeSetting;
		enum calibratationType _calType;
		int _channel;
		int _oldVoltage;
		int _oldPreVoltage;
		int _oldCurrent;
		emgl::Text *_titleLabel;
		emgl::Text *_instructionLabel;
		emgl::Text *_dacSettingLabel;
		emgl::Text *_actualReadingLabel;
		EditableValue *_dacSetting;
		EditableValue *_reading;
		
		std::map<int,int> _calDACPoints;
		std::map<int,int> _calADCPoints;
	};
}

#endif /* CALIBRATIONPANEL_H_ */