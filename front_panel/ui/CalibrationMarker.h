#ifndef CALIBRATIONMARKER_H_
#define CALIBRATIONMARKER_H_

#include <emgl/emgl_ui.h>

namespace ui {
	class CalibrationMarker : public emgl::Element
	{
		protected:
		emgl_color_t _foregroundColor;

		public:
		CalibrationMarker(emgl::Element *parent, emgl::Point pos, emgl::Size size, int id = -1);
		virtual ~CalibrationMarker();

		void setForeground(emgl_color_t color);

		emgl_color_t getForeground() const;

		virtual void Draw(emgl::PaintContext &PC, emgl::Rectangle &area);
	};
}


#endif