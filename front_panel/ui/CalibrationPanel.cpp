/*
 * CalibrationPanel.cpp
 *
 * Created: 02-09-17 17:24:25
 *  Author: Robojan
 */ 

#include "CalibrationPanel.h"
#include "../utils/log.h"
#include "../fonts/Luna18.h"
#include "../fonts/PTMonoBoldNumeric22.h"
#include "../tasks.h"
#include <emgl/debug.h>
#include <cstring>
using namespace ui;



CalibrationPanel::CalibrationPanel(emgl::Element *parent, emgl::Point pos, emgl::Size size, int channel, enum calibratationType calType) : 
	emgl::Panel(parent, pos, size), _activeSetting(SET_setting), _calType(calType), _channel(channel)
{
	EMGL_ASSERT("Channel number is larger than the max num of channels",
		channel <= 2 && channel > 0);
	
	
	emgl_length_t width = size.width - 100;
	
	_oldVoltage = getChannelVoltage(_channel - 1);
	_oldPreVoltage = getChannelPreVoltage(_channel - 1);
	_oldCurrent = getChannelCurrent(_channel - 1);
	
	int maxValue;
	int minValue;
	char unit[2];
	char titleText[35];
	switch(calType){
	case CAL_VOLTAGE:
		strcpy(titleText, "Channel 0 Voltage Calibration");
		maxValue = 35000;
		minValue = 0;
		unit[0] = 'V';
		setChannelPreVoltage(_channel - 1, 300);
		setChannelCurrent(_channel - 1, 1000);
		setChannelRawVoltage(_channel - 1, 0);
		setOutputEnabled(_channel - 1, 1);
		break;
	case CAL_CURRENT:
		strcpy(titleText, "Channel 0 Current Calibration");
		maxValue = 15000;
		minValue = 0;
		unit[0] = 'A';
		setChannelPreVoltage(_channel - 1, 300);
		setChannelRawCurrent(_channel - 1, 0);
		setChannelVoltage(_channel - 1, 1000);
		setOutputEnabled(_channel - 1, 1);
		break;
	default:
		EMGL_ASSERT("Invallid calibration type", 1==0);
		return;
	}
	unit[1] = '\0';
	titleText[8] += channel;
	
	_titleLabel = new emgl::Text(this, emgl::Point(10, size.height - 50), emgl::Size(width-20, 50), titleText, -1, 
		emgl::ALIGN_CENTER, emgl::ALIGN_CENTER, true);
	
	_instructionLabel = new emgl::Text(this, emgl::Point(10, size.height - 100), emgl::Size(width-20, 40), 
		"Add calibration points over the complete range", -1);
	
	_dacSettingLabel = new emgl::Text(this, emgl::Point(10, size.height -  140), emgl::Size(150, 30), "Setting: ");
	_actualReadingLabel = new emgl::Text(this, emgl::Point(10, size.height -  190), emgl::Size(150, 30), "Actual value: ");
	_dacSetting = new EditableValue(this, emgl::Point(170, size.height -  140), emgl::Size(200,20), 0, 4);
	_dacSetting->setMax(4095);
	_dacSetting->setMin(0);
	_dacSetting->setFont(&font_PTMonoBoldNumeric22);
	_dacSetting->setCallback((void(*)(EditableValue*, int, void*))onSettingCallback, this);
	_reading = new EditableValue(this, emgl::Point(170, size.height - 190), emgl::Size(200,30), 0, 5, unit, 2);
	_reading->setMax(maxValue);
	_reading->setMin(0);
	_reading->setFont(&font_PTMonoBoldNumeric22);
	_dacSetting->setActive(_activeSetting == SET_setting);
	_reading->setActive(_activeSetting == SET_readout);	
}

CalibrationPanel::~CalibrationPanel() {
	setOutputEnabled(_channel - 1, 0);
	setChannelPreVoltage(_channel - 1, _oldPreVoltage);
	setChannelVoltage(_channel - 1, _oldVoltage);
	setChannelCurrent(_channel - 1, _oldCurrent);
}

void CalibrationPanel::setBackground(emgl_color_t color)
{
	Panel::setBackground(color);
	_titleLabel->setBackground(color);
	_instructionLabel->setBackground(color);
	_dacSettingLabel->setBackground(color);
	_actualReadingLabel->setBackground(color);
	_dacSetting->setBackground(color);
	_reading->setBackground(color);
}

void CalibrationPanel::setForeground(emgl_color_t color)
{
	Panel::setForeground(color);
	_titleLabel->setForeground(color);
	_dacSettingLabel->setForeground(color);
	_actualReadingLabel->setForeground(color);
	_dacSetting->setForeground(color);
	_reading->setForeground(color);
}

void CalibrationPanel::ChangeSetting()
{
	if(_activeSetting == SET_setting) return;
	_activeSetting = SET_setting;
	_dacSetting->setActive(_activeSetting == SET_setting);
	_reading->setActive(_activeSetting == SET_readout);
	_dacSetting->Refresh();
	_reading->Refresh();
}

void CalibrationPanel::ChangeReadout()
{
	if(_activeSetting == SET_readout) return;
	_activeSetting = SET_readout;
	_dacSetting->setActive(_activeSetting == SET_setting);
	_reading->setActive(_activeSetting == SET_readout);
	_dacSetting->Refresh();
	_reading->Refresh();
}

void CalibrationPanel::AddPoint()
{
	int output = _reading->getValue();
	int setting = _dacSetting->getValue();
	int adcValue;
	switch(_calType) {
	case CAL_VOLTAGE:
		adcValue = g_dispVoltage[_channel - 1];
		break;
	case CAL_CURRENT:
		adcValue = g_dispCurrent[_channel - 1];
		break;
	}
	logMsg("Add calibration point %d = %d\n", setting, output);
	_calDACPoints[output] = setting;
	_calADCPoints[adcValue] = output;
}

void CalibrationPanel::PrevDigit()
{
	switch(_activeSetting) {
	case SET_setting:
		_dacSetting->PrevDigit();
		break;
	case SET_readout:
		_reading->PrevDigit();
		break;
	}
}

void CalibrationPanel::NextDigit()
{
	switch(_activeSetting) {
		case SET_setting:
		_dacSetting->NextDigit();
		break;
		case SET_readout:
		_reading->NextDigit();
		break;
	}
}

void CalibrationPanel::ModifyDigit(int change)
{
	switch(_activeSetting) {
		case SET_setting:
		_dacSetting->modifyDigit(change);
		break;
		case SET_readout:
		_reading->modifyDigit(change);
		break;
	}
}

void CalibrationPanel::onSettingCallback(EditableValue *sender, int value, CalibrationPanel *self)
{
	int preVoltage;
	switch(self->_calType) {
	case CAL_VOLTAGE:
		preVoltage = getVoltageFromRawValue(value) + 300;
		setChannelPreVoltage(self->_channel - 1, preVoltage);
		setChannelRawVoltage(self->_channel - 1, value);
		break;
	case CAL_CURRENT:
		setChannelRawCurrent(self->_channel - 1, value);
		break;
	}
}

std::map<int,int> &CalibrationPanel::GetCalibrationADCPoints()
{
	return _calADCPoints;	
}

std::map<int,int> &CalibrationPanel::GetCalibrationDACPoints()
{
	return _calDACPoints;	
}