/*
 * StatusPanel.cpp
 *
 * Created: 02-09-17 17:34:15
 *  Author: Robojan
 */ 

#include "StatusPanel.h"
#include "../utils/stringhelper.h"

using namespace ui;


StatusPanel::StatusPanel(emgl::Element *parent, emgl::Point pos, emgl::Size size) :
emgl::Panel(parent, pos, size)
{
	_outEn[0] = false;
	_outEn[1] = false;
	_tempLabel = new emgl::Text(this, emgl::Point(0, 0),
	emgl::Size(-1,size.height), "20.0\xb0""C", -1, emgl::ALIGN_LEFT,
	emgl::ALIGN_CENTER, false);
	
	_output1 = new emgl::Text(this, emgl::Point(100, 0), emgl::Size(80,size.height),
	"Output 1:", -1, emgl::ALIGN_RIGHT, emgl::ALIGN_CENTER, false);
	_output2 = new emgl::Text(this, emgl::Point(225, 0), emgl::Size(80,size.height),
	"Output 2:", -1, emgl::ALIGN_RIGHT, emgl::ALIGN_CENTER, false);
}

StatusPanel::~StatusPanel()
{
}

void StatusPanel::setBackground(emgl_color_t color)
{
	Panel::setBackground(color);
	_tempLabel->setBackground(color);
	_output1->setBackground(color);
	_output2->setBackground(color);
}

void StatusPanel::DrawEnabledSymbol(emgl::Point pos, emgl::PaintContext &PC, emgl::Rectangle &area)
{
	static const emgl_coord_t checkXValues[] = {1, 7, 17, 15, 7, 3};
	static const emgl_coord_t checkYValues[] = {7, 1, 14, 16, 5, 9};
	PC.drawRectangleBezelLoweredFilled(emgl::Rectangle(pos, emgl::Size(20, 20)), emgl_colorDarker(getBackground()));
	PC.drawFilledPolygon(checkXValues, checkYValues, 6, pos, COLOR_LIME);
}

void StatusPanel::DrawDisabledSymbol(emgl::Point pos, emgl::PaintContext &PC, emgl::Rectangle &area)
{
	static const emgl_coord_t crossXValues[] = {1, 3, 9, 15, 17, 11, 17, 16, 9, 3, 1, 7};
	static const emgl_coord_t crossYValues[] = {3, 1, 7, 1, 3, 9, 15, 16, 11, 17, 15, 9};
	PC.drawRectangleBezelLoweredFilled(emgl::Rectangle(pos, emgl::Size(20, 20)), emgl_colorDarker(getBackground()));
	PC.drawFilledPolygon(crossXValues, crossYValues, 12, pos, COLOR_RED);
}

void StatusPanel::Draw(emgl::PaintContext &PC, emgl::Rectangle &area)
{
	emgl::Panel::Draw(PC, area);
	if(_outEn[0]) {
		DrawEnabledSymbol(emgl::Point(185, 8), PC, area);
		} else {
		DrawDisabledSymbol(emgl::Point(185, 8), PC, area);
	}
	if(_outEn[1]) {
		DrawEnabledSymbol(emgl::Point(310, 8), PC, area);
		} else {
		DrawDisabledSymbol(emgl::Point(310, 8), PC, area);
	}
}

void StatusPanel::update(int temp, bool ch1_outen, bool ch2_outen)
{
	//_tempLabel->setText(getTempText(temp));
	char workingBuffer[12];
	_tempLabel->setText(getFixedPointNumberWithUnits(temp, 1, "\xb0""C", workingBuffer, 12));
	_tempLabel->Refresh();
	emgl::Rectangle updateRect(185,8,20,20);
	emgl::PaintContext PC(GetScreenPos(), GetSize());
	if(ch1_outen != _outEn[0])
	{
		_outEn[0] = ch1_outen;
		emgl::Element::Refresh(PC, updateRect);
	}
	if(ch2_outen != _outEn[1])
	{
		updateRect.x = 310;
		_outEn[1] = ch2_outen;
		emgl::Element::Refresh(PC, updateRect);
	}
}
