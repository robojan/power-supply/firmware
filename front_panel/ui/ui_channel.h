/*
 * ui_channel.h
 *
 * Created: 2-3-2017 17:34:23
 *  Author: Robojan
 */ 


#ifndef UI_CHANNEL_H_
#define UI_CHANNEL_H_

#include <emgl/emgl_ui.h>
#include "EditableValue.h"

namespace ui {

class Channel : public emgl::Panel
{
public:
	enum editingField {
		VoltageSetting,
		CurrentSetting
	};
	Channel(emgl::Element *parent, emgl::Point pos, emgl::Size size, int channel);
	virtual ~Channel();

	virtual void setBackground(emgl_color_t color);


	virtual void update(int voltage, int current, bool cc);

	void setActive(bool active);
	void PrevSettingDigit();
	void NextSettingDigit();
	void switchSetting();
	void RefreshSettingBar(enum editingField field);
	void modifyDigit(int delta);
	void setCallback(void (*func)(int, enum editingField, int));
	void setSetting(enum editingField field, int value);
	
	void onSettingCallback(EditableValue *sender, int value);
protected:
	int _channel;
private:
	void (*_callbackFunc)(int channel, enum editingField field, int setting);

	int _voltage;
	int _current;

	bool _active;
	enum editingField _editingField;
	emgl_color_t _bgColor;
	emgl::Text *_label;
	emgl::Text *_cccvlabel;
	emgl::Text *_VReading;
	emgl::Text *_AReading;
	EditableValue _VSetting;
	EditableValue _ASetting;
};


}



#endif /* UI_CHANNEL_H_ */