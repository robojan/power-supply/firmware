/*
 * CalibrationMarker.cpp
 *
 * Created: 02-09-17 17:19:27
 *  Author: Robojan
 */ 

#include "CalibrationMarker.h"
using namespace ui;

CalibrationMarker::CalibrationMarker(emgl::Element *parent, emgl::Point pos, emgl::Size size, int id) :
	emgl::Element(parent, pos, size, id), _foregroundColor(COLOR_BLACK)
{
	
}

CalibrationMarker::~CalibrationMarker() {

}

void CalibrationMarker::setForeground(emgl_color_t color) {
	_foregroundColor = color;
	Refresh();
}

emgl_color_t CalibrationMarker::getForeground() const {
	return _foregroundColor;
}

void CalibrationMarker::Draw(emgl::PaintContext &PC, emgl::Rectangle &area)
{
	(void) area;
	emgl::Size size = GetSize();
	PC.drawCircle(emgl::Point(size.width/2,size.height/2), size.width/2 * 4 / 5 ,
	_foregroundColor);
	PC.drawLine(emgl::Point(0, size.height/2), emgl::Point(size.width/2-4, size.height/2), _foregroundColor);
	PC.drawLine(emgl::Point(size.width, size.height/2), emgl::Point(size.width/2+4, size.height/2), _foregroundColor);
	PC.drawLine(emgl::Point(size.width/2, 0), emgl::Point(size.width/2, size.height/2-4), _foregroundColor);
	PC.drawLine(emgl::Point(size.width/2, size.height/2+4), emgl::Point(size.width/2, size.height), _foregroundColor);
	PC.setPixel(emgl::Point(size.width/2, size.height/2), _foregroundColor);
}