/*
 * ui_channel.cpp
 *
 * Created: 2-3-2017 17:33:45
 *  Author: Robojan
 */ 

#include "ui_channel.h"
#include <emgl/debug.h>
#include "../utils/log.h"
#include "../utils/stringhelper.h"
#include "../fonts/PTMonoBoldNumeric36.h"
#include "../fonts/PTMonoBoldNumeric22.h"

namespace ui {

static const char * const channelLabels[] = {
	"Channel 1:",
	"Channel 2:"
};
static const char * const CCCVLabels[] = {
	"CV",
	"CC"
};

static void channelEditableValueCallback(ui::EditableValue *sender, int value, void *user)
{
	ui::Channel *channel = (ui::Channel *)user;
	channel->onSettingCallback(sender, value);
}

ui::Channel::Channel(emgl::Element *parent, emgl::Point pos, emgl::Size size, 
	int channel) : emgl::Panel(parent, pos, size), _channel(channel),
	_active(false), _editingField(VoltageSetting), 
	_callbackFunc(NULL),
	_ASetting(NULL,  emgl::Point(220, size.height-110), emgl::Size(120, 20), 0, 4, "A", 1, -1,
	emgl::ALIGN_RIGHT, emgl::ALIGN_BOTTOM), 
	_VSetting(NULL, emgl::Point(220, size.height-70), emgl::Size(120, 20), 0, 4, "V", 2, -1,
		emgl::ALIGN_RIGHT, emgl::ALIGN_BOTTOM)
{
	EMGL_ASSERT("Channel number is larger than the max num of channels", 
		channel <= 2 && channel > 0);
	_voltage = 2888;
	_current = 1040;
	_label = new emgl::Text(this, emgl::Point(0, size.height-25), 
		emgl::Size(-1,-1), channelLabels[_channel-1], -1, emgl::ALIGN_LEFT, 
		emgl::ALIGN_TOP, false);
	_cccvlabel = new emgl::Text(this, emgl::Point(260, size.height-35), 
		emgl::Size(50,35), CCCVLabels[0], -1, emgl::ALIGN_LEFT, 
		emgl::ALIGN_BOTTOM, false);
	_cccvlabel->setFont(&font_PTMonoBoldNumeric36);
	_VReading = new emgl::Text(this, emgl::Point(0, size.height-70), 
		emgl::Size(210,40), "28.88V", -1, emgl::ALIGN_RIGHT, 
		emgl::ALIGN_BOTTOM, false);
	_VReading->setFont(&font_PTMonoBoldNumeric36);
	_AReading = new emgl::Text(this, emgl::Point(0, size.height-110), 
		emgl::Size(210,40), "1.04A", -1, emgl::ALIGN_RIGHT, 
		emgl::ALIGN_BOTTOM, false);
	_AReading->setFont(&font_PTMonoBoldNumeric36);
	emgl::Element::AddChild(&_VSetting, false);
	emgl::Element::AddChild(&_ASetting, false);
	_VSetting.setFont(&font_PTMonoBoldNumeric22);
	_ASetting.setFont(&font_PTMonoBoldNumeric22);
	_VSetting.setMax(3000);
	_ASetting.setMax(1500);
	_VSetting.setMin(0);
	_ASetting.setMin(0);
	_VSetting.setCallback(channelEditableValueCallback, this);
	_ASetting.setCallback(channelEditableValueCallback, this);
	setActive(false);
}

ui::Channel::~Channel()
{

}

void Channel::setBackground(emgl_color_t color)
{
	if(color == _bgColor) return;
	_bgColor = color;
	setActive(_active);
}

void Channel::update(int voltage, int current, bool cc)
{
	char workingBuffer[12];
	_voltage = voltage;
	_current = current;
	_VReading->setText(getFixedPointNumberWithUnits(voltage, 3, "V", workingBuffer, 12));
	_AReading->setText(getFixedPointNumberWithUnits(current, 3, "A", workingBuffer, 12));
	_cccvlabel->setText(CCCVLabels[cc?1:0]);
	_VReading->Refresh();
	_AReading->Refresh();
	_cccvlabel->Refresh();
}

void Channel::setActive(bool active)
{
	emgl_color_t bgColor = active ? emgl_colorDarker(_bgColor) : emgl_colorDoubleDarker(_bgColor); 
	emgl::Panel::setBackground(bgColor);
	_label->setBackground(bgColor);
	_cccvlabel->setBackground(bgColor);
	_VReading->setBackground(bgColor);
	_AReading->setBackground(bgColor);
	_VSetting.setBackground(bgColor);
	_ASetting.setBackground(bgColor);
	_active = active;
	_VSetting.setActive(active && _editingField == VoltageSetting);
	_ASetting.setActive(active && _editingField == CurrentSetting);
}

void Channel::PrevSettingDigit()
{
	if(_active && _editingField == VoltageSetting) _VSetting.PrevDigit();
	if(_active && _editingField == CurrentSetting) _ASetting.PrevDigit();
}

void Channel::NextSettingDigit()
{
	if(_active && _editingField == VoltageSetting) _VSetting.NextDigit();
	if(_active && _editingField == CurrentSetting) _ASetting.NextDigit();
}

void Channel::switchSetting()
{
	switch(_editingField) {
	case VoltageSetting:
		_editingField = CurrentSetting; break;
	case CurrentSetting:
		_editingField = VoltageSetting; break;
	}
	_VSetting.setActive(_active && _editingField == VoltageSetting);
	_ASetting.setActive(_active && _editingField == CurrentSetting);
	_VSetting.Refresh();
	_ASetting.Refresh();
}

void Channel::modifyDigit(int delta)
{
	switch(_editingField)
	{
	case VoltageSetting:
		_VSetting.modifyDigit(delta);
		break;
	case CurrentSetting:
		_ASetting.modifyDigit(delta);
		break;
	}
}

void Channel::setCallback(void (*func)(int, enum editingField, int))
{
	_callbackFunc = func;
}

void Channel::setSetting(enum editingField field, int value)
{
	switch(field)
	{
	case VoltageSetting:
		_VSetting.setValue(value);
		break;
	case CurrentSetting:
		_ASetting.setValue(value);
		break;
	}
}

void Channel::onSettingCallback(EditableValue *sender, int value)
{
	if(!_callbackFunc) return;
	if(sender == &_VSetting) {
		_callbackFunc(_channel, VoltageSetting, value);
	} else if(sender == &_ASetting) {
		_callbackFunc(_channel, CurrentSetting, value);
	}
}
}