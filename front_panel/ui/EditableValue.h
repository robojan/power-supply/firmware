
#ifndef EDITABLEVALUE_H_
#define EDITABLEVALUE_H_

#include <emgl/emgl_ui.h>

namespace ui {
	class EditableValue: public emgl::Text
	{
		public:
		EditableValue(emgl::Element *parent, emgl::Point pos, emgl::Size size,
		int defaultValue, int digits, const char *unit = "", int decimalPlace = -1, int id = -1,
		emgl::Alignment hAlign = emgl::ALIGN_LEFT, emgl::Alignment vAlign = emgl::ALIGN_BOTTOM);
		virtual ~EditableValue();
		
		virtual void setValue(int value);
		virtual int getValue() const;
		virtual void setActive(bool active);
		virtual void Draw(emgl::PaintContext &PC, emgl::Rectangle &area);
		
		void setMin(int min);
		void setMax(int max);
		void PrevDigit();
		void NextDigit();
		void modifyDigit(int delta);
		void setCallback(void (*func)(EditableValue *, int, void *user), void *user);
		private:
		int _min;
		int _max;
		int _nDigits;
		int _decimalPlace;
		bool _active;
		int _editingPos;
		void (*_callback)(EditableValue *, int, void *user);
		void *_user;
	};
}

#endif
