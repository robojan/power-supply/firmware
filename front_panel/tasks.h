/*
 * tasks.h
 *
 * Created: 18-2-2017 00:24:31
 *  Author: Robojan
 */ 

#ifndef TASKS_H_
#define TASKS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

// Task priorities
#define TASK_PRIORITY_GRAPHICS   1
#define TASK_PRIORITY_USERINPUT  1
#define TASK_PRIORITY_ACTUATION  4
#define TASK_PRIORITY_SENSING    4


// Notification bits
#define TASK_NOTIFY_GRAPHICS_BUTTONS 0x1
#define TASK_NOTIFY_GRAPHICS_TOUCH 0x2
#define TASK_NOTIFY_GRAPHICS_QEC 0x4

// Task periods
#define TASK_PERIOD_USERINPUT    50
#define TASK_TIMEOUT_GRAPHICS    20
#define TASK_PERIOD_ACTUATION    10
#define TASK_PERIOD_SENSING      20

// Task stack sizes
#define TASK_STACKSIZE_GRAPHICS  512
#define TASK_STACKSIZE_USERINPUT 256
#define TASK_STACKSIZE_ACTUATION 150
#define TASK_STACKSIZE_SENSING   256


typedef struct {
	int temp:1;
	struct {
		int adc:1;
		int dacv:1;
		int daci:1;
		int prepot:1;
	} ch[2];
} system_status_t;

// Shared variables
extern SemaphoreHandle_t g_ts_val_semaphore;
extern uint8_t g_buttonState;
extern uint8_t g_ts_pressed;
extern uint16_t g_ts_x;
extern uint16_t g_ts_y;
extern int32_t g_qec_pos;
extern int32_t g_qec_dpos;
extern int g_temperature;
extern int g_dispCurrent[2];
extern int g_instVoltage[2];
extern int g_dispVoltage[2];
extern int g_decayVoltage[2];
extern uint8_t g_cc[2];
extern system_status_t g_status;

// Task handles
extern xTaskHandle g_graphics_taskHandle;
extern xTaskHandle g_userInput_taskHandle;
extern xTaskHandle g_actuation_taskHandle;
extern xTaskHandle g_sensing_taskHandle;

// Task creation
void createGraphicsTask();
void createUITask();
void createActuationTask();
void createSensingTask();

// Task definitions
void graphicsTask(void *parameters);
void uiTask(void *parameters);
void actuationTask(void *parameters);
void sensingTask(void *parameters);



enum UIState {
	STATE_init,
	STATE_mainWindow,
	STATE_settings,
	STATE_confirmCalib,
	STATE_calibSettings,
	STATE_cal_step1,
	STATE_cal_step2,
	STATE_cal_step3,
	STATE_cal_step4,
	STATE_chcal_vcal,
	STATE_chcal_ical,
};

void setNextUIState(enum UIState nextState);
void refreshWindow();
void startTSCalibration();
int isTSCalibrated();
void setBacklight(int brightness);
void setFanSpeed(int speed);
int getFanSpeed();
void setChannelPreVoltage(int channel, int voltage);
int getChannelPreVoltage(int channel);
void setPowerLimitVoltage(int channel, int voltage);
void setOutputEnabled(int channel, int enabled);
int getOutputEnabled(int channel);
void setChannelVoltage(int channel, int voltage);
int getChannelVoltage(int channel);
void setChannelCurrent(int channel, int current);
int getChannelCurrent(int channel);
void EmergencyShutdown();

int getVoltageFromRawValue(int value);
uint16_t getRawValueFromVoltage(int voltage);
int getCurrentFromRawValue(int value);
uint16_t getRawValueFromCurrent(int voltage);
void setChannelRawVoltage(int channel, uint16_t value);
void setChannelRawCurrent(int channel, uint16_t value);

#ifdef __cplusplus
}
#endif

#endif /* TASKS_H_ */