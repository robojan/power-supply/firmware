/*
 * calibration.c
 *
 * Created: 03-09-17 15:11:30
 *  Author: Robojan
 */ 

#include "settings.h"
#include <stdlib.h>
#include <avr32/io.h>
#include "utils/log.h"
#include "utils/crc.h"
#include <flashc.h>
#include <string.h>
#include "tasks.h"

struct calibrationPoint_s {
	int16_t dacSetting;
	int16_t adcReadout;
	int16_t value;
};

struct calibrationData_s {
	uint8_t numPoints;
	struct calibrationPoint_s *points;
};

static struct settings_channel_s{
	int16_t voltage;
	int16_t current;
} g_channel_setting[2];
static struct calibrationData_s g_voltage_calibration[2] = {0, NULL};
static struct calibrationData_s g_current_calibration[2] = {0, NULL};

#define MAX_SETTINGS_SIZE (AVR32_FLASHC_USER_PAGE_SIZE - 8)
#define SETTINGS ((void *) AVR32_FLASHC_USER_PAGE_ADDRESS)
#define SETTINGS_CRC_ADDR (AVR32_FLASHC_USER_PAGE_ADDRESS + MAX_SETTINGS_SIZE)
#define SETTINGS_CRC (*((crc_t *) SETTINGS_CRC_ADDR))


int isAddrInUserPage(void *addr)
{
	uint32_t a = (uint32_t) addr;
	uint32_t page = a & ~(AVR32_FLASHC_USER_PAGE_SIZE-1);
	return page == AVR32_FLASHC_USER_PAGE_ADDRESS;
}

void DestroyCalibrationData(struct calibrationData_s *data) 
{
	if(data->points != NULL) {
		if(!isAddrInUserPage(data->points)) {
			free(data->points);	
		}
		data->numPoints = 0;
		data->points = NULL;
	}
}

int AreSettingsValid() 
{
	crc_t crc = crc_init();
	crc = crc_update(crc, SETTINGS, MAX_SETTINGS_SIZE);
	crc = crc_finalize(crc);
	logMsg("Calculated CRC: %08x, saved CRC: %08x\n", crc, SETTINGS_CRC);
	return crc == SETTINGS_CRC;
}

int LoadSettings()
{
	for(int i = 0; i < 2; i++){
		DestroyCalibrationData(&g_voltage_calibration[i]);
		DestroyCalibrationData(&g_current_calibration[i]);
		g_channel_setting[i].current = 100;
		g_channel_setting[i].voltage = 500;
	}
	if(AreSettingsValid()) {
		logMsg("Settings are valid\n");
		memcpy(g_channel_setting, (void *)AVR32_FLASHC_USER_PAGE, sizeof(g_channel_setting));
		uint32_t flash_calib_address = AVR32_FLASHC_USER_PAGE_ADDRESS + sizeof(g_channel_setting);
		g_voltage_calibration[0].numPoints = *((uint8_t *)(flash_calib_address + 0));
		g_current_calibration[0].numPoints = *((uint8_t *)(flash_calib_address + 1));
		g_voltage_calibration[1].numPoints = *((uint8_t *)(flash_calib_address + 2));
		g_current_calibration[1].numPoints = *((uint8_t *)(flash_calib_address + 3));
		uint32_t flash_calib_data_address = flash_calib_address + 4;
		for(int i = 0; i< 2; i++) {
			if(g_voltage_calibration[i].numPoints > 0) {
				g_voltage_calibration[i].points = (struct calibrationPoint_s *)(flash_calib_data_address);	
			} else {
				g_voltage_calibration[i].points = NULL;
			}
			flash_calib_data_address += g_voltage_calibration[i].numPoints * sizeof(struct calibrationPoint_s);
			if(g_current_calibration[i].numPoints > 0) {
				g_current_calibration[i].points = (struct calibrationPoint_s *)(flash_calib_data_address);	
			} else {
				g_current_calibration[i].points = NULL;
			}
			flash_calib_data_address += g_current_calibration[i].numPoints * sizeof(struct calibrationPoint_s);
		}
	} else {
		logMsg("Settings are invalid\n");
		SaveSettings();
	}
}

uint8_t *WriteDataWordAndCalculateCRC(uint8_t *addr, void *src, int len, uint32_t *tmpBuffer, crc_t *crc)
{
	int byteAddr = (int)(((uint32_t)addr) & 3);
	uint8_t *byteSrc = (uint8_t *)src;
	if(byteAddr != 0) {
		for(int i = byteAddr; i < 4 && i - byteAddr < len; i++) {
			*tmpBuffer |= *byteSrc << (24 - (i * 8));
			byteSrc++;
			len--;
			addr++;
		}
		if((int)(((uint32_t)addr) & 3) == 0) {
			*((uint32_t *)(addr - 4)) = *tmpBuffer;
			*crc = crc_update(*crc, tmpBuffer, 4);
			*tmpBuffer = 0;
		}
	} else {
		*tmpBuffer = 0;
	}
	while(len >= 4) {
		uint32_t value = (byteSrc[0] << 24) | (byteSrc[1] << 16) | (byteSrc[2] << 8) | (byteSrc[3] << 0);
		*((uint32_t *)addr) = value;
		*crc = crc_update(*crc, &value, 4);
		addr += 4;
		len -= 4;
		byteSrc += 4;
	}
	while(len > 0) {
		*tmpBuffer |= *byteSrc << (24 - (((int)(((uint32_t)addr) & 3)) * 8));
		addr++;
		len--;
		byteSrc++;
	}
	return addr;
}

uint8_t *FinalizeSettingsAndCalculateCRC(uint8_t *addr, uint32_t *tmpBuffer, crc_t *crc)
{
	uint8_t *endPtr = (uint8_t *)(AVR32_FLASHC_USER_PAGE_ADDRESS + MAX_SETTINGS_SIZE);
	if(addr >= (uint8_t *)endPtr) {
		*crc = crc_finalize(*crc);
		return addr;
	}
	uint8_t byteAddr = (uint8_t)(((uint32_t)addr) & 3);
	if(byteAddr != 0) {
		for(int i = byteAddr; i < 4; i++) {
			*tmpBuffer |= 0xFF << (24 - (i * 8));
			addr++;
		}
		*((uint32_t *)(addr - 4)) = *tmpBuffer;
		*crc = crc_update(*crc, tmpBuffer, 4);
		*tmpBuffer = 0;
	}
	int len = (int)(endPtr - addr);
	log_assert((len & 3) == 0, "(len & 3) == 0");
	uint32_t val = 0xFFFFFFFF;
	while(len > 0) {
		*((uint32_t *)addr) = val;
		*crc = crc_update(*crc, &val, sizeof(uint32_t));
		len -= 4;
		addr += 4;
	}
	*crc = crc_finalize(*crc);
	return addr;
}

int SaveSettings()
{
	struct settings_channel_s *flash_channelSettings = (struct settings_channel_s *)AVR32_FLASHC_USER_PAGE;
	int settingsSize = sizeof(g_channel_setting) + sizeof(g_voltage_calibration[0].numPoints) +  sizeof(g_current_calibration[0].numPoints) +
		sizeof(g_voltage_calibration[1].numPoints) +  sizeof(g_current_calibration[1].numPoints) +
		g_voltage_calibration[0].numPoints * sizeof(struct calibrationPoint_s) +  g_current_calibration[0].numPoints * sizeof(struct calibrationPoint_s) +
		g_voltage_calibration[1].numPoints * sizeof(struct calibrationPoint_s) +  g_current_calibration[1].numPoints * sizeof(struct calibrationPoint_s);
	log_assert(settingsSize < MAX_SETTINGS_SIZE, "Settings size is too large");
	unsigned int bootloaderConfig = GetBootloaderConfig();
	int i = 0;
	for(i= 0; i< 5; i++) {
		logMsg("Erasing user page ... ");
		if(flashc_erase_user_page(true) == false) {
			logMsg("Failed, retrying\n");
		} else {
			logMsg("Success\n");
			break;
		}
	}
	if(i == 5) {
		logMsg("Error failed to erase flash\n");
		return false;
	}
	flashc_clear_page_buffer();
	uint8_t *writePtr = (uint8_t *)AVR32_FLASHC_USER_PAGE;
	uint32_t tmpBuffer;
	crc_t crc = crc_init();
	writePtr = WriteDataWordAndCalculateCRC(writePtr, g_channel_setting, sizeof(g_channel_setting), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, &g_voltage_calibration[0].numPoints, sizeof(g_voltage_calibration[0].numPoints), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, &g_current_calibration[0].numPoints, sizeof(g_current_calibration[0].numPoints), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, &g_voltage_calibration[1].numPoints, sizeof(g_voltage_calibration[1].numPoints), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, &g_current_calibration[1].numPoints, sizeof(g_current_calibration[1].numPoints), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, g_voltage_calibration[0].points, g_voltage_calibration[0].numPoints * sizeof(struct calibrationPoint_s), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, g_current_calibration[0].points, g_current_calibration[0].numPoints * sizeof(struct calibrationPoint_s), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, g_voltage_calibration[1].points, g_voltage_calibration[1].numPoints * sizeof(struct calibrationPoint_s), &tmpBuffer, &crc);
	writePtr = WriteDataWordAndCalculateCRC(writePtr, g_current_calibration[1].points, g_current_calibration[1].numPoints * sizeof(struct calibrationPoint_s), &tmpBuffer, &crc);	
	writePtr = FinalizeSettingsAndCalculateCRC(writePtr, &tmpBuffer, &crc);
	SETTINGS_CRC = crc;
	*((unsigned int *)(AVR32_FLASHC_USER_PAGE + AVR32_FLASHC_USER_PAGE_SIZE - sizeof(unsigned int))) = bootloaderConfig;
	
	flashc_write_user_page();
	logMsg("Settings written to flash\n");
	logMsg("Repointing calibration pointers");
	uint32_t flash_calib_address = AVR32_FLASHC_USER_PAGE_ADDRESS + sizeof(g_channel_setting);
	uint32_t flash_calib_data_address = flash_calib_address + 4;
	for(int i = 0; i< 2; i++) {
		if(g_voltage_calibration[i].numPoints > 0 && !isAddrInUserPage(g_voltage_calibration[i].points)) {
			if(g_voltage_calibration[i].points != NULL) free(g_voltage_calibration[i].points);
			g_voltage_calibration[i].points = (struct calibrationPoint_s *)(flash_calib_data_address);	
		}
		flash_calib_data_address += g_voltage_calibration[i].numPoints * sizeof(struct calibrationPoint_s);
		if(g_current_calibration[i].numPoints > 0 && !isAddrInUserPage(g_current_calibration[i].points)) {
			if(g_current_calibration[i].points != NULL) free(g_current_calibration[i].points);
			g_current_calibration[i].points = (struct calibrationPoint_s *)(flash_calib_data_address);	
		}
		flash_calib_data_address += g_current_calibration[i].numPoints * sizeof(struct calibrationPoint_s);
	}
}

void StoreVoltageCalibration(int channelId, int numPoints, int16_t *voltage, int16_t *setting, int16_t *adcValues)
{
	log_assert(channelId >= 0 && channelId < 2, "channelId >= 0 && channelId < 2");
	DestroyCalibrationData(&g_voltage_calibration[channelId]);
	g_voltage_calibration[channelId].points = malloc(sizeof(struct calibrationPoint_s) * numPoints);
	g_voltage_calibration[channelId].numPoints = numPoints;
	for(int i = 0; i < numPoints; i++) {
		g_voltage_calibration[channelId].points[i].value = voltage[i];
		g_voltage_calibration[channelId].points[i].dacSetting = setting[i];
		g_voltage_calibration[channelId].points[i].adcReadout = adcValues[i];
	}
}

void StoreCurrentCalibration(int channelId, int numPoints, int16_t *current, int16_t *setting, int16_t *adcValues)
{	
	log_assert(channelId >= 0 && channelId < 2, "channelId >= 0 && channelId < 2");
	DestroyCalibrationData(&g_current_calibration[channelId]);
	g_current_calibration[channelId].points = malloc(sizeof(struct calibrationPoint_s) * numPoints);
	g_current_calibration[channelId].numPoints = numPoints;
	for(int i = 0; i < numPoints; i++) {
		g_current_calibration[channelId].points[i].value = current[i];
		g_current_calibration[channelId].points[i].dacSetting = setting[i];
		g_current_calibration[channelId].points[i].adcReadout = adcValues[i];
	}
}

const unsigned char *GetSerialNumber() {
	return (const unsigned char *)AVR32_FLASHC_SERIAL_DATA;
}

int GetSerialLength() {
	return AVR32_FLASHC_SERIAL_DATA_SIZE;
}

unsigned int GetBootloaderConfig() {
	unsigned int config = *((unsigned int *)(AVR32_FLASHC_USER_PAGE + AVR32_FLASHC_USER_PAGE_SIZE - sizeof(unsigned int)));
	return config != 0xFFFFFFFF ? config : 0x929E0D6B;
}

void PrintSettings() {
	logMsg("Settings:\n");
	const unsigned char *serial = GetSerialNumber();
	log_assert(GetSerialLength() == 15, "Expected serial length to be 15");
	logMsg("\tSerial: %02x%02x%02x%02x%02x-%02x%02x%02x%02x%02x-%02x%02x%02x%02x%02x\n", 
		serial[0], serial[1], serial[2], serial[3], serial[4], 
		serial[5], serial[6], serial[7], serial[8], serial[9],
		serial[10], serial[11], serial[12], serial[13], serial[14]);
	logMsg("\tBootloader config: %08x\n", GetBootloaderConfig());
	for(int i = 0; i < 2; i++) {
		logMsg("\tChannel %d:\n\t\tVoltage: %d\n\t\tCurrent: %d\n", i, 
			g_channel_setting[i].voltage, g_channel_setting[i].current);
		logMsg("\t\tVoltage calibration data: \n");
		for(int j = 0; j < g_voltage_calibration[i].numPoints; j++) 
		{
			logMsg("\t\t\t%5d - %04d - %d\n", g_voltage_calibration[i].points[j].value, 
				g_voltage_calibration[i].points[j].dacSetting, 
				g_voltage_calibration[i].points[j].adcReadout);	
		}
		logMsg("\t\tCurrent calibration data: \n");
		for(int j = 0; j < g_current_calibration[i].numPoints; j++)
		{
			logMsg("\t\t\t%5d - %04d - %d\n", g_current_calibration[i].points[j].value,
			g_current_calibration[i].points[j].dacSetting,
			g_current_calibration[i].points[j].adcReadout);
		}		
	}
	logMsg("User page: \n");
	logHexDump(AVR32_FLASHC_USER_PAGE, AVR32_FLASHC_USER_PAGE_SIZE);
}

int16_t GetCalibratedADCValue(int16_t adcReading, const struct calibrationData_s *calib)
{
	if(calib->numPoints < 2 || calib->points == NULL) {
		return adcReading;
	}
	int highIdx;
	for(highIdx = 0; highIdx < calib->numPoints; highIdx++) {
		if(adcReading < calib->points[highIdx].adcReadout) {
			break;
		}
	}
	int lowIdx = highIdx - 1;
	if(lowIdx < 0) {
		// Outside calibration range
		lowIdx = 0;
		highIdx = 1;
	}
	if(highIdx >= calib->numPoints) {
		// Outside calibration range
		lowIdx = calib->numPoints - 2;
		highIdx = calib->numPoints - 1;
	}
	int lowValue = calib->points[lowIdx].value;
	int highValue = calib->points[highIdx].value;
	int lowAdcValue = calib->points[lowIdx].adcReadout;
	int highAdcValue = calib->points[highIdx].adcReadout;
	int dy = highValue - lowValue;
	int dx = highAdcValue - lowAdcValue;
	float a = ((float)dy)/((float)dx);
	float b = lowValue - a * lowAdcValue;
	float y = a * adcReading + b + 0.5f;
	return (int16_t)y;
}

int16_t GetCalibratedDACValue(int16_t value, const struct calibrationData_s *calib)
{
	if(calib->numPoints < 2 || calib->points == NULL) {
		return -1;
	}
	int highIdx;
	for(highIdx = 0; highIdx < calib->numPoints; highIdx++) {
		if(value < calib->points[highIdx].value) {
			break;
		}
	}
	int lowIdx = highIdx - 1;
	if(lowIdx < 0) {
		// Outside calibration range
		lowIdx = 0;
		highIdx = 1;
	}
	if(highIdx >= calib->numPoints) {
		// Outside calibration range
		lowIdx = calib->numPoints - 2;
		highIdx = calib->numPoints - 1;
	}
	int lowValue = calib->points[lowIdx].dacSetting;
	int highValue = calib->points[highIdx].dacSetting;
	int lowDacValue = calib->points[lowIdx].value;
	int highDacValue = calib->points[highIdx].value;
	int dy = highValue - lowValue;
	int dx = highDacValue - lowDacValue;
	float a = ((float)dy)/((float)dx);
	float b = lowValue - a * lowDacValue;
	float y = a * value + b + 0.5f;
	return (int16_t)y;
}

int16_t GetCalibratedADCVoltage(int channel, int16_t adcReading)
{
	return GetCalibratedADCValue(adcReading, &g_voltage_calibration[channel]);
}

int16_t GetCalibratedADCCurrent(int channel, int16_t adcReading)
{
	return GetCalibratedADCValue(adcReading, &g_current_calibration[channel]);
}

int16_t GetCalibratedDACVoltage(int channel, int16_t value)
{
	int16_t val = GetCalibratedDACValue(value * 10, &g_voltage_calibration[channel]);
	if(val < 0) {
		return getRawValueFromVoltage(value);
	}
	return val;
}

int16_t GetCalibratedDACCurrent(int channel, int16_t value)
{
	int16_t val = GetCalibratedDACValue(value, &g_current_calibration[channel]);
	if(val < 0) {
		return getRawValueFromCurrent(value);
	}
	return val;
}

int GetBootVoltage(int channel)
{
	return g_channel_setting[channel].voltage;
}

int GetBootCurrent(int channel)
{
	return g_channel_setting[channel].current;
}