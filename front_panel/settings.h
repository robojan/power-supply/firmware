/*
 * calibration.h
 *
 * Created: 03-09-17 13:52:07
 *  Author: Robojan
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

int LoadSettings();
int SaveSettings();
void StoreVoltageCalibration(int channelId, int numPoints, int16_t *voltage, int16_t *setting, int16_t *adcValues);
void StoreCurrentCalibration(int channelId, int numPoints, int16_t *current, int16_t *setting, int16_t *adcValues);
const unsigned char *GetSerialNumber();
int GetSerialLength();
unsigned int GetBootloaderConfig();
void PrintSettings();

int16_t GetCalibratedADCVoltage(int channel, int16_t adcReading);
int16_t GetCalibratedADCCurrent(int channel, int16_t adcReading);
int16_t GetCalibratedDACVoltage(int channel, int16_t value);
int16_t GetCalibratedDACCurrent(int channel, int16_t value);
int GetBootVoltage(int channel);
int GetBootCurrent(int channel);

#ifdef __cplusplus
};
#endif



#endif /* SETTINGS_H_ */