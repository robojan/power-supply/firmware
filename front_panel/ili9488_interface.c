/*
* ili9488_interface.c
*
* Created: 2-4-2017 17:44:29
*  Author: Robojan
*/
#include "ili9488_interface.h"
#include <avr32/io.h>
#include <emgl/emgl.h>
#include <board.h>
#include <FreeRTOS.h>
#include <task.h>
#include <gpio.h>
#include <stdint.h>

#define SET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrs = 1<<((x)%32)
#define RESET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrc = 1<<((x)%32)

void lcd_setDatabusInput() __attribute__((optimize("-O3")));
void lcd_setDatabusOutput() __attribute__((optimize("-O3")));
void lcd_setDatabus(uint16_t data) __attribute__((optimize("-O3")));
uint16_t lcd_getDatabus() __attribute__((optimize("-O3")));
void lcd_fillWrite(emgl_U16 cmd, emgl_U16 data, emgl_U32 n)  __attribute__((optimize("-O3")));

static const gpio_map_t g_lcd_gpiomap = {
	{LCD_D0, 0},
	{LCD_D1, 0},
	{LCD_D2, 0},
	{LCD_D3, 0},
	{LCD_D4, 0},
	{LCD_D5, 0},
	{LCD_D6, 0},
	{LCD_D7, 0},
	{LCD_D8, 0},
	{LCD_D9, 0},
	{LCD_D10, 0},
	{LCD_D11, 0},
	{LCD_D12, 0},
	{LCD_D13, 0},
	{LCD_D14, 0},
	{LCD_D15, 0},
	{LCD_RST, 0},
	{LCD_RS, 0},
	{LCD_RD, 0},
	{LCD_WR, 0},
	{LCD_CS, 0},
};

void lcd_setDatabusInput()
{
	uint32_t port[AVR32_GPIO_PORT_LENGTH] = {0};
	port[LCD_D0/32] |= 1<<(LCD_D0 % 32);
	port[LCD_D1/32] |= 1<<(LCD_D1 % 32);
	port[LCD_D2/32] |= 1<<(LCD_D2 % 32);
	port[LCD_D3/32] |= 1<<(LCD_D3 % 32);
	port[LCD_D4/32] |= 1<<(LCD_D4 % 32);
	port[LCD_D5/32] |= 1<<(LCD_D5 % 32);
	port[LCD_D6/32] |= 1<<(LCD_D6 % 32);
	port[LCD_D7/32] |= 1<<(LCD_D7 % 32);
	port[LCD_D8/32] |= 1<<(LCD_D8 % 32);
	port[LCD_D9/32] |= 1<<(LCD_D9 % 32);
	port[LCD_D10/32] |= 1<<(LCD_D10 % 32);
	port[LCD_D11/32] |= 1<<(LCD_D11 % 32);
	port[LCD_D12/32] |= 1<<(LCD_D12 % 32);
	port[LCD_D13/32] |= 1<<(LCD_D13 % 32);
	port[LCD_D14/32] |= 1<<(LCD_D14 % 32);
	port[LCD_D15/32] |= 1<<(LCD_D15 % 32);
	for(int i = 0; i<AVR32_GPIO_PORT_LENGTH; i++)
	{
		AVR32_GPIO.port[i].oderc = port[i];
	}
}

void lcd_setDatabusOutput()
{
	uint32_t port[AVR32_GPIO_PORT_LENGTH] = {0};
	port[LCD_D0/32] |= 1<<(LCD_D0 % 32);
	port[LCD_D1/32] |= 1<<(LCD_D1 % 32);
	port[LCD_D2/32] |= 1<<(LCD_D2 % 32);
	port[LCD_D3/32] |= 1<<(LCD_D3 % 32);
	port[LCD_D4/32] |= 1<<(LCD_D4 % 32);
	port[LCD_D5/32] |= 1<<(LCD_D5 % 32);
	port[LCD_D6/32] |= 1<<(LCD_D6 % 32);
	port[LCD_D7/32] |= 1<<(LCD_D7 % 32);
	port[LCD_D8/32] |= 1<<(LCD_D8 % 32);
	port[LCD_D9/32] |= 1<<(LCD_D9 % 32);
	port[LCD_D10/32] |= 1<<(LCD_D10 % 32);
	port[LCD_D11/32] |= 1<<(LCD_D11 % 32);
	port[LCD_D12/32] |= 1<<(LCD_D12 % 32);
	port[LCD_D13/32] |= 1<<(LCD_D13 % 32);
	port[LCD_D14/32] |= 1<<(LCD_D14 % 32);
	port[LCD_D15/32] |= 1<<(LCD_D15 % 32);
	for(int i = 0; i<AVR32_GPIO_PORT_LENGTH; i++)
	{
		AVR32_GPIO.port[i].oders = port[i];
	}
}

void lcd_setDatabus(uint16_t data)
{
	// Mask is calculated at compile time.
	uint32_t mask[AVR32_GPIO_PORT_LENGTH] = {0};
	mask[LCD_D0/32] |= 1<<(LCD_D0 % 32);
	mask[LCD_D1/32] |= 1<<(LCD_D1 % 32);
	mask[LCD_D2/32] |= 1<<(LCD_D2 % 32);
	mask[LCD_D3/32] |= 1<<(LCD_D3 % 32);
	mask[LCD_D4/32] |= 1<<(LCD_D4 % 32);
	mask[LCD_D5/32] |= 1<<(LCD_D5 % 32);
	mask[LCD_D6/32] |= 1<<(LCD_D6 % 32);
	mask[LCD_D7/32] |= 1<<(LCD_D7 % 32);
	mask[LCD_D8/32] |= 1<<(LCD_D8 % 32);
	mask[LCD_D9/32] |= 1<<(LCD_D9 % 32);
	mask[LCD_D10/32] |= 1<<(LCD_D10 % 32);
	mask[LCD_D11/32] |= 1<<(LCD_D11 % 32);
	mask[LCD_D12/32] |= 1<<(LCD_D12 % 32);
	mask[LCD_D13/32] |= 1<<(LCD_D13 % 32);
	mask[LCD_D14/32] |= 1<<(LCD_D14 % 32);
	mask[LCD_D15/32] |= 1<<(LCD_D15 % 32);
	 
	// Do some bitshifting to align all the bits
	 
	uint32_t data32 = (uint32_t)data;
	uint32_t dataRev32 = data32;
	__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
	uint32_t pa = dataRev32 & 0xC0000000;
	pa |= (dataRev32 << 4) & 0x0F000000;
	uint32_t pb = (dataRev32 >> 8) & 0x00000F00;
	pa |= (dataRev32 >> 21) & 0x000001F8;
	AVR32_GPIO.port[0].ovrc = mask[0];
	AVR32_GPIO.port[0].ovrs = pa;
	AVR32_GPIO.port[1].ovrc = mask[1];
	AVR32_GPIO.port[1].ovrs = pb;
}

uint16_t lcd_getDatabus()
{
	uint32_t pa = AVR32_GPIO.port[0].pvr;
	uint32_t pb = AVR32_GPIO.port[1].pvr;
	uint32_t data = pa & 0xC0000000;
	data |= (pa << 21) & 0x3F000000;
	data |= (pa >> 4) & 0x00F00000;
	data |= (pb << 8) & 0x000F0000;
	__asm__("brev\t%0" : "+r"(data) : : "cc");
	return (uint16_t)data;
}
 
void lcd_io_init()
{
	for(int i = 0; i < 21; i++)
	{
		gpio_enable_gpio_pin(g_lcd_gpiomap[i].pin);
		gpio_enable_pin_glitch_filter(g_lcd_gpiomap[i].pin);
		gpio_disable_pin_interrupt(g_lcd_gpiomap[i].pin);
		gpio_disable_pin_pull_up(g_lcd_gpiomap[i].pin);
	}
	lcd_setDatabusInput();
	AVR32_GPIO.port[(LCD_RST) / 32].oders = 1<<((LCD_RST)%32);
	AVR32_GPIO.port[(LCD_RS) / 32].oders = 1<<((LCD_RS)%32);
	AVR32_GPIO.port[(LCD_WR) / 32].oders = 1<<((LCD_WR)%32);
	AVR32_GPIO.port[(LCD_RD) / 32].oders = 1<<((LCD_RD)%32);
	AVR32_GPIO.port[(LCD_CS) / 32].oders = 1<<((LCD_CS)%32);
	SET_PIN(LCD_RST);
	SET_PIN(LCD_RS);
	SET_PIN(LCD_WR);
	SET_PIN(LCD_RD);
	SET_PIN(LCD_CS);
}

void lcd_writeCmdStart(emgl_U16 cmd) 
{
	RESET_PIN(LCD_RS);
	RESET_PIN(LCD_CS);
	AVR32_GPIO.port[0].oders = 0xCF0001F8;
	AVR32_GPIO.port[1].oders = 0x00000F00;
	RESET_PIN(LCD_WR);	
	// Do some bitshifting to align all the bits	
	uint32_t data32 = (uint32_t)cmd;
	uint32_t dataRev32 = data32;
	__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
	uint32_t pa = dataRev32 & 0xC0000000;
	pa |= (dataRev32 << 4) & 0x0F000000;
	uint32_t pb = (dataRev32 >> 8) & 0x00000F00;
	pa |= (dataRev32 >> 21) & 0x000001F8;
	AVR32_GPIO.port[0].ovrc = 0xCF0001F8;
	AVR32_GPIO.port[0].ovrs = pa;
	AVR32_GPIO.port[1].ovrc = 0x00000F00;
	AVR32_GPIO.port[1].ovrs = pb;
	SET_PIN(LCD_WR);
	SET_PIN(LCD_RS);
}

void lcd_fillWrite(emgl_U16 cmd, emgl_U16 data, emgl_U32 n)
{
	RESET_PIN(LCD_RS);
	RESET_PIN(LCD_CS);
	AVR32_GPIO.port[0].oders = 0xCF0001F8;
	AVR32_GPIO.port[1].oders = 0x00000F00;
	RESET_PIN(LCD_WR);
	// Do some bitshifting to align all the bits
	uint32_t data32 = (uint32_t)cmd;
	uint32_t dataRev32 = data32;
	__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
	uint32_t pa = dataRev32 & 0xC0000000;
	pa |= (dataRev32 << 4) & 0x0F000000;
	uint32_t pb = (dataRev32 >> 8) & 0x00000F00;
	pa |= (dataRev32 >> 21) & 0x000001F8;
	AVR32_GPIO.port[0].ovrc = 0xCF0001F8;
	AVR32_GPIO.port[0].ovrs = pa;
	AVR32_GPIO.port[1].ovrc = 0x00000F00;
	AVR32_GPIO.port[1].ovrs = pb;
	SET_PIN(LCD_WR);
	SET_PIN(LCD_RS);
	// Do some bitshifting to align all the bits
	data32 = (uint32_t)data;
	dataRev32 = data32;
	__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
	pa = dataRev32 & 0xC0000000;
	pa |= (dataRev32 << 4) & 0x0F000000;
	pb = (dataRev32 >> 8) & 0x00000F00;
	pa |= (dataRev32 >> 21) & 0x000001F8;
	AVR32_GPIO.port[0].ovrc = 0xCF0001F8;
	AVR32_GPIO.port[0].ovrs = pa;
	AVR32_GPIO.port[1].ovrc = 0x00000F00;
	AVR32_GPIO.port[1].ovrs = pb;
	for(emgl_U32 i = 0; i < n; i++) 
	{
		RESET_PIN(LCD_WR);
		SET_PIN(LCD_WR);
	}

	lcd_setDatabusInput();
	SET_PIN(LCD_CS);
}

void lcd_writeCmdEnd()
{
	lcd_setDatabusInput();
	SET_PIN(LCD_CS);
}

void lcd_writeData(emgl_U16 data)
{
	RESET_PIN(LCD_WR);
	lcd_setDatabus(data);
	SET_PIN(LCD_WR);
}

void lcd_writeCmd(emgl_U16 cmd, emgl_U32 n, const emgl_U16 *data)
{
	RESET_PIN(LCD_RS);
	RESET_PIN(LCD_CS);
	AVR32_GPIO.port[0].oders = 0xCF0001F8;
	AVR32_GPIO.port[1].oders = 0x00000F00;
	RESET_PIN(LCD_WR);
	uint32_t data32 = (uint32_t)cmd;
	uint32_t dataRev32 = data32;
	__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
	uint32_t pa = dataRev32 & 0xC0000000;
	pa |= (dataRev32 << 4) & 0x0F000000;
	uint32_t pb = (dataRev32 >> 8) & 0x00000F00;
	pa |= (dataRev32 >> 21) & 0x000001F8;
	AVR32_GPIO.port[0].ovrc = 0xCF0001F8;
	AVR32_GPIO.port[0].ovrs = pa;
	AVR32_GPIO.port[1].ovrc = 0x00000F00;
	AVR32_GPIO.port[1].ovrs = pb;
	SET_PIN(LCD_WR);
	SET_PIN(LCD_RS);
	while(n > 0) {
		RESET_PIN(LCD_WR);
		data32 = (uint32_t)*data;
		dataRev32 = data32;
		__asm__("brev\t%0" : "+r"(dataRev32) : : "cc");
		pa = dataRev32 & 0xC0000000;
		pa |= (dataRev32 << 4) & 0x0F000000;
		pb = (dataRev32 >> 8) & 0x00000F00;
		pa |= (dataRev32 >> 21) & 0x000001F8;
		AVR32_GPIO.port[0].ovrc = 0xCF0001F8;
		AVR32_GPIO.port[0].ovrs = pa;
		AVR32_GPIO.port[1].ovrc = 0x00000F00;
		AVR32_GPIO.port[1].ovrs = pb;
		SET_PIN(LCD_WR);
		data++;
		n--;
	}
	AVR32_GPIO.port[0].oderc = 0xCF0001F8;
	AVR32_GPIO.port[1].oderc = 0x00000F00;
	SET_PIN(LCD_CS);
}

void lcd_readCmd(emgl_U16 cmd, emgl_U32 n, emgl_U16 *data)
{
	RESET_PIN(LCD_RS);
	RESET_PIN(LCD_CS);
	lcd_setDatabusOutput();
	RESET_PIN(LCD_WR);
	lcd_setDatabus(cmd);
	SET_PIN(LCD_WR);
	SET_PIN(LCD_RS);
	lcd_setDatabusInput();
	RESET_PIN(LCD_RD);
	portNOP();
	SET_PIN(LCD_RD);
	while(n > 0) {
		portNOP();
		portNOP();
		portNOP();
		portNOP();
		portNOP();
		portNOP();
		portNOP();
		RESET_PIN(LCD_RD);
		*data = lcd_getDatabus();
		SET_PIN(LCD_RD);
		n--;
		data++;
	}
	SET_PIN(LCD_CS);
}

void lcd_reset()
{
	RESET_PIN(LCD_RST);
	vTaskDelay(10 * portTICK_PERIOD_MS);
	SET_PIN(LCD_RST);
	vTaskDelay(120* portTICK_PERIOD_MS);
}