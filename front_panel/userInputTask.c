/*
 * userInputTask.c
 *
 * Created: 18-2-2017 22:20:10
 *  Author: Robojan
 */

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include "board.h"
#include "tasks.h"
#include <gpio.h>
#include <usart.h>
#include <intc.h>
#include <pdca.h>
#include <eic.h>
#include "delay.h"
 
#define TS_RESPONSE_SUCCESS            0x00
#define TS_RESPONSE_CMD_UNKNOWN        0x01
#define TS_RESPONSE_HEADER_UNKNOWN     0x03
#define TS_RESPONSE_TIMEOUT            0x04
#define TS_RESPONSE_CANCEL_CALIBRATION 0xFC

#define TS_GET_VERSION                 0x10
#define TS_ENABLE_TOUCH                0x12
#define TS_DISABLE_TOUCH               0x13
#define TS_CALIBRATE_MODE              0x14
#define TS_REG_READ                    0x20
#define TS_REG_WRITE                   0x21
#define TS_REG_START_ADDR_REQUEST      0x22
#define TS_REG_WRITE_TO_EEPROM         0x23
#define TS_EEPROM_READ                 0x28
#define TS_EEPROM_WRITE                0x29
#define TS_EEPROM_WRITE_TO_REG         0x2B

#define SET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrs = 1<<((x)%32)
#define RESET_PIN(x) AVR32_GPIO.port[(x) / 32].ovrc = 1<<((x)%32)
#define GET_PIN(x) ((AVR32_GPIO.port[(x) / 32].pvr >> ((x)%32)) & 1)

#define TS_BUFFER_SIZE 16

static int g_qec_state = 0;
static int g_qec_steps = 0;

typedef struct {
	uint8_t pen;
	uint16_t x;
	uint16_t y;
} touch_message_t;

typedef struct {
	struct {
		uint8_t major;
		uint8_t minor;
	} version;
	uint8_t resolution;
	uint8_t type;
	uint16_t reg_start_addr;
} ts_info_t;

static portBASE_TYPE ts_irq_command();
static portBASE_TYPE ts_irq_touchMessage();

static StaticSemaphore_t g_ts_rx_semaphore_buffer[2];
static SemaphoreHandle_t g_ts_cmd_semaphore = NULL;
static StaticSemaphore_t g_ts_val_semaphore_buffer;
SemaphoreHandle_t g_ts_val_semaphore = NULL;
static volatile int g_initialized = 0;
static portBASE_TYPE (*g_ts_rx_func)() = ts_irq_command;
static uint8_t *g_ts_cmd_buffer = NULL;
static uint8_t g_ts_cmd_buffer_size = 0;
static touch_message_t g_ts_msgs_buffer[TS_BUFFER_SIZE];
static uint8_t g_ts_msgs_read_pos = 0;
static uint8_t g_ts_msgs_write_pos = 0;
static SemaphoreHandle_t g_ts_rx_semaphore = NULL;
static uint8_t g_ts_rx_buffer[5];
static uint8_t g_ts_rx_write_pos = 0;

static ts_info_t g_ts_info;

uint8_t g_buttonState = 0xFF;
int32_t g_qec_pos = 0;
int32_t g_qec_dpos = 0;
uint8_t g_ts_cal_mode = 0;
uint8_t g_ts_cmdMode = 0;
uint8_t g_ts_pressed = false;
uint16_t g_ts_x = 0;
uint16_t g_ts_y = 0;

static const uint8_t g_qec_nextstate[4][4] = {{0,1,2,0},{0,1,1,3},{0,2,2,3},{3,1,2,3}};
static const int8_t g_qec_inc[4][4] = {{0,-1,1,0},{1,0,0,-1},{-1,0,0,1},{0,1,-1,0}};

static StaticTask_t g_userInput_task;
static StackType_t g_userInput_stack[TASK_STACKSIZE_USERINPUT];
TaskHandle_t g_userInput_taskHandle = NULL;

static portBASE_TYPE qec_irq_nonNaked()
{
	gpio_toggle_pin(AVR32_PIN_PB07);
	uint8_t newState = ((AVR32_GPIO.port[0].pvr & QEC_MASK)>>QEC_OFFSET) & 3;
	//logMsg("QEC: %d\n", newState);
	
	g_qec_steps += g_qec_inc[g_qec_state][newState];
	g_qec_state = g_qec_nextstate[g_qec_state][newState];
	
	gpio_clear_pin_interrupt_flag(QEC_A);
	gpio_clear_pin_interrupt_flag(QEC_B);
	return pdFALSE;
}

static portBASE_TYPE ts_irq_command()
{
	if(TS_USART.IMR.rxrdy) {
		uint8_t val = TS_USART.rhr & 0xFF;
		if(!TS_USART.CSR.frame){
			// Frame error
			TS_USART.CR.rststa = 1;
			return pdFALSE;
		}
		if(g_ts_cmd_buffer_size > 0 && g_ts_cmd_buffer != NULL) {
			*g_ts_cmd_buffer++ = val;
			g_ts_cmd_buffer_size--;
			if(g_ts_cmd_buffer_size == 0) {
				portBASE_TYPE higherPriorityTaskWoken;
				portENTER_CRITICAL();
				xSemaphoreGiveFromISR(g_ts_cmd_semaphore, &higherPriorityTaskWoken);
				portEXIT_CRITICAL();
				return higherPriorityTaskWoken;
			}
		}
	}
	return pdFALSE;
}

static portBASE_TYPE ts_irq_touchMessage()
{
	if(TS_USART.IMR.rxrdy) {
		uint8_t val = TS_USART.rhr & 0xFF;
		if(!TS_USART.CSR.frame){
			// Frame error
			TS_USART.CR.rststa = 1;
			g_ts_rx_write_pos = 0;
			return pdFALSE;
		}
		if(g_ts_rx_write_pos == 0) {
			// Check for valid starting byte
			if(val != 0x55 && (val & (1<<7)) != 0) {
				g_ts_rx_buffer[g_ts_rx_write_pos] = val;
				g_ts_rx_write_pos++;
			}
		} else {
			if((val & (1<<8)) != 0) {
				// Invalid message
				g_ts_rx_write_pos = 0;
			} else {
				g_ts_rx_buffer[g_ts_rx_write_pos] = val;
				g_ts_rx_write_pos++;
				if(g_ts_rx_write_pos == 5) {
					portBASE_TYPE higherPriorityTaskWoken = pdFALSE;
					// Complete message received. 
					g_ts_rx_write_pos = 0;
					portENTER_CRITICAL();
					if(xSemaphoreGiveFromISR(g_ts_rx_semaphore, &higherPriorityTaskWoken))
					{
						// Parse the message
						touch_message_t *msg = &g_ts_msgs_buffer[g_ts_msgs_write_pos];
						msg->pen = g_ts_rx_buffer[0] & 0x01;
						msg->x = (g_ts_rx_buffer[1] & 0x7F) | ((g_ts_rx_buffer[2] & 0x1F) << 7);
						msg->y = (g_ts_rx_buffer[3] & 0x7F) | ((g_ts_rx_buffer[4] & 0x1F) << 7);
											
						g_ts_msgs_write_pos = (g_ts_msgs_write_pos + 1) & (TS_BUFFER_SIZE - 1);
					}
					portEXIT_CRITICAL();
					return higherPriorityTaskWoken;
				}
			}
		}
	}
	return pdFALSE;
}

static __attribute__((__naked__)) void ts_irq()
{
	portENTER_SWITCHING_ISR();
	g_ts_rx_func();
	portEXIT_SWITCHING_ISR();
}

static __attribute__((__naked__)) void qec_irq()
{
	portENTER_SWITCHING_ISR();
	qec_irq_nonNaked();
	portEXIT_SWITCHING_ISR();
}

static Bool getTSMessageIdx(touch_message_t *msg, int blocktime)
{
	if(xSemaphoreTake(g_ts_rx_semaphore, blocktime) == pdTRUE)
	{
		const touch_message_t *bufferMsg = &g_ts_msgs_buffer[g_ts_msgs_read_pos];
		msg->pen = bufferMsg->pen;
		msg->x = bufferMsg->x;
		msg->y = bufferMsg->y;
		g_ts_msgs_read_pos = (g_ts_msgs_read_pos + 1) & (TS_BUFFER_SIZE - 1);
		return true;
	}
	return false;
}

static void setupPinMapping()
{
	gpio_enable_module_pin(TS_RXD, TS_RXD_FUNC);
	gpio_enable_module_pin(TS_TXD, TS_TXD_FUNC);
	gpio_enable_module_pin(QEC_A, QEC_A_FUNC);
	gpio_enable_gpio_pin(SW_CLK);
	gpio_enable_gpio_pin(SW_LATCH);
	gpio_enable_gpio_pin(SW_DATA);
	gpio_enable_gpio_pin(QEC_B);
	AVR32_GPIO.port[SW_CLK/32].oders = 1 << (SW_CLK % 32);
	AVR32_GPIO.port[SW_LATCH/32].oders = 1 << (SW_LATCH % 32);
	AVR32_GPIO.port[SW_DATA/32].oderc = 1 << (SW_DATA % 32);
	AVR32_GPIO.port[QEC_B/32].oderc = 1 << (QEC_B % 32);
	RESET_PIN(SW_CLK);
	SET_PIN(SW_LATCH);
}

static void sendTSCmd(uint8_t cmd, uint8_t n, const uint8_t *data)
{
	portENTER_CRITICAL();
	usart_putchar(&TS_USART, 0x55);
	usart_putchar(&TS_USART, n + 1);
	TS_USART.CR.rststa = 1;
	int dummyread = TS_USART.rhr;
	(void) dummyread;
	usart_putchar(&TS_USART, cmd);
	while(n > 0) 
	{
		usart_putchar(&TS_USART, *data);
		data++;
		n--;
	}
	portEXIT_CRITICAL();
}

static void ts_setCommandMode()
{
	if(g_ts_cmdMode) {
		return;
	}


	uint8_t buffer[4];
	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = buffer;
	
	sendTSCmd(TS_DISABLE_TOUCH, 0, NULL);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || buffer[1] != 2 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_DISABLE_TOUCH) {
			portDBG_TRACE("Error receiving TS disable touch message. Len: %d, response: %02x, cmd: %02x", buffer[1]+2, response, buffer[3]);
			return;
		}
	}
	g_ts_cmdMode = 1;
	vTaskDelay(50*portTICK_PERIOD_MS);
}

static void ts_setTouchMode()
{
	if(!g_ts_cmdMode) {
		return;
	}
	uint8_t buffer[4];
	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = buffer;
	
	sendTSCmd(TS_ENABLE_TOUCH, 0, NULL);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || buffer[1] + 2 != 4 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_ENABLE_TOUCH) {
			portDBG_TRACE("Error receiving TS enable touch message. Len: %d, response: %02x, cmd: %02x", buffer[1] + 2, response, buffer[3]);
			//return;
		}
	}
	g_ts_cmdMode = 0;

	g_ts_rx_func = ts_irq_touchMessage;
	
}

static void ts_getInfo(ts_info_t *info) {
	uint8_t buffer[7];
	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 7;
	g_ts_cmd_buffer = buffer;

	sendTSCmd(TS_GET_VERSION, 0, NULL);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 7 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_GET_VERSION) {
			portDBG_TRACE("Error receiving TS get info message. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return;
		}
	}
	info->version.major = buffer[4];
	info->version.minor = buffer[5];
	info->resolution = 8 + 2 * ((buffer[6]>>6)&3);
	info->type = buffer[6] & 0x3F;
	
	info->reg_start_addr = 0; // default;
	g_ts_cmd_buffer_size = 5;
	g_ts_cmd_buffer = buffer;
	sendTSCmd(TS_REG_START_ADDR_REQUEST, 0, NULL);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 5 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_REG_START_ADDR_REQUEST) {
			portDBG_TRACE("Error receiving TS get register start address message. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return;
		}

	}
	info->reg_start_addr = buffer[4];
}

static void ts_writeReg(uint8_t addr, uint8_t val)
{
	uint8_t buffer[4];
	ts_setCommandMode();

	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = buffer;
	uint16_t addrVal = g_ts_info.reg_start_addr + addr;
	buffer[0] = (addrVal >> 8) & 0xFF;
	buffer[1] = addrVal & 0xFF;
	buffer[2] = 1;
	buffer[3] = val;
	sendTSCmd(TS_REG_WRITE, 4, buffer);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 4 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_REG_WRITE) {
			portDBG_TRACE("Error receiving TS reg write response. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return;
		}
	}
}

static uint8_t ts_readReg(uint8_t addr)
{
	uint8_t buffer[5];
	ts_setCommandMode();

	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 5;
	g_ts_cmd_buffer = buffer;
	uint16_t addrVal = g_ts_info.reg_start_addr + addr;
	buffer[0] = (addrVal >> 8) & 0xFF;
	buffer[1] = addrVal & 0xFF;
	buffer[2] = 1;
	sendTSCmd(TS_REG_READ, 3, buffer);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 5 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_REG_READ) {
			portDBG_TRACE("Error receiving TS reg read response. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return 0;
		}
		return buffer[4];
	}
	return 0;
}

static uint8_t ts_readEeprom(uint8_t addr)
{
	uint8_t buffer[5];
	ts_setCommandMode();

	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 5;
	g_ts_cmd_buffer = buffer;
	uint16_t addrVal = g_ts_info.reg_start_addr + addr;
	buffer[0] = (addrVal >> 8) & 0xFF;
	buffer[1] = addrVal & 0xFF;
	buffer[2] = 1;
	sendTSCmd(TS_EEPROM_READ, 3, buffer);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 5 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_EEPROM_READ) {
			portDBG_TRACE("Error receiving TS eeprom read response. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return 0;
		}
		return buffer[4];
	}
	return 0;
}

static int ts_readAllRegs(uint8_t *data, int len)
{
	ts_setCommandMode();
	int i;
	for(i = 0; i < 19 && i < len; i++) {
		data[i] = ts_readReg(i);
	}
	return i;
}

static int ts_readAllEeprom(uint8_t *data, int len)
{
	ts_setCommandMode();
	int i;
	for(i = 0; i < 256 && i < len; i++) {
		data[i] = ts_readEeprom(i);
	}
	return i;
}

static void ts_startCalibration() {
	uint8_t buffer[4];
	ts_setCommandMode();
	g_ts_rx_func = ts_irq_command;

	uint16_t addr = g_ts_info.reg_start_addr + 0xe;
	buffer[0] = (addr >> 8) & 0xFF;
	buffer[1] = addr & 0xFF;
	buffer[2] = 1;
	buffer[3] = 25;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = buffer;
	sendTSCmd(TS_REG_WRITE, 4, buffer);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = buffer[1] + 2;
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 4 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_REG_WRITE) {
			portDBG_TRACE("Error receiving TS reg write response. Len: %d, response: %02x, cmd: %02x", len, response, buffer[3]);
			return;
		}
	}

	buffer[0] = 0x04;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = g_ts_rx_buffer;
	sendTSCmd(TS_CALIBRATE_MODE, 1, buffer);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		int len = g_ts_rx_buffer[1] + 2;
		uint8_t response = g_ts_rx_buffer[2];
		if(g_ts_cmd_buffer_size != 0 || len != 4 || response != TS_RESPONSE_SUCCESS || g_ts_rx_buffer[3] != TS_CALIBRATE_MODE) {
			portDBG_TRACE("Error receiving TS calibration message. Len: %d, response: %02x, cmd: %02x", len, response, g_ts_rx_buffer[3]);
			return;
		}
	}
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = g_ts_rx_buffer;

}

static void ts_regs_write_to_eeprom() 
{
	uint8_t buffer[4];
	ts_setCommandMode();

	g_ts_rx_func = ts_irq_command;
	g_ts_cmd_buffer_size = 4;
	g_ts_cmd_buffer = buffer;
	
	sendTSCmd(TS_REG_WRITE_TO_EEPROM, 0, NULL);
	if(xSemaphoreTake(g_ts_cmd_semaphore, 50 * portTICK_PERIOD_MS))
	{
		uint8_t response = buffer[2];
		if(g_ts_cmd_buffer_size != 0 || buffer[1] != 2 || response != TS_RESPONSE_SUCCESS || buffer[3] != TS_REG_WRITE_TO_EEPROM) {
			portDBG_TRACE("Error receiving TS disable touch message. Len: %d, response: %02x, cmd: %02x", buffer[1]+2, response, buffer[3]);
			return;
		}
	}
}

int isTSCalibrated() {
	while(!g_initialized);
	int calibrated =  ts_readReg(0x0D) & 0x01;
	ts_setTouchMode();
	return calibrated;
}

static void setNextTSCalState() {
	logMsg("[Input] Next calibration step\n\r");
	switch(g_ts_cal_mode) {
	case 1:
		setNextUIState(STATE_cal_step2);
		g_ts_cal_mode = 2;
		g_ts_cmd_buffer_size = 4;
		g_ts_cmd_buffer = g_ts_rx_buffer;
		break;
	case 2:
		setNextUIState(STATE_cal_step3);
		g_ts_cal_mode = 3;
		g_ts_cmd_buffer_size = 4;
		g_ts_cmd_buffer = g_ts_rx_buffer;
		break;
	case 3:
		setNextUIState(STATE_cal_step4);
		g_ts_cal_mode = 4;
		g_ts_cmd_buffer_size = 4;
		g_ts_cmd_buffer = g_ts_rx_buffer;
		break;
	case 4:	
		g_ts_cal_mode = 5;
		g_ts_cmd_buffer_size = 4;
		g_ts_cmd_buffer = g_ts_rx_buffer;
		break;
	case 5:
		ts_regs_write_to_eeprom();
		setNextUIState(STATE_mainWindow);
		ts_setTouchMode();
		g_ts_cal_mode = 0;
		break;
	}
}

static void initTouchScreen()
{	
	g_ts_rx_semaphore = xSemaphoreCreateCountingStatic(TS_BUFFER_SIZE, 0, &g_ts_rx_semaphore_buffer[0]);
	g_ts_cmd_semaphore = xSemaphoreCreateBinaryStatic(&g_ts_rx_semaphore_buffer[1]);
	
	const usart_options_t usart_options = {
		.baudrate = 9600,
		.charlength = 8,
		.paritytype = USART_NO_PARITY,
		.stopbits = 1,
		.channelmode = USART_NORMAL_CHMODE,
	};
	usart_init_rs232(&TS_USART, &usart_options, FPBA);
	
	INTC_register_interrupt(ts_irq, TS_IRQ, 0);
	TS_USART.IER.rxrdy = 1;
		
	vTaskDelay(50 * portTICK_PERIOD_MS);

	ts_setCommandMode();

	ts_getInfo(&g_ts_info);
	portDBG_TRACE("Touch screen controller: %d.%d %d-bit (%02x) start=%02x", g_ts_info.version.major, 
		g_ts_info.version.minor, g_ts_info.resolution, g_ts_info.type, g_ts_info.reg_start_addr);

	uint8_t regs[19];
	int len;
	if((len = ts_readAllRegs(regs, 19)) != 19) {
		portDBG_TRACE("Could not read all TS registers: %d", len);
	} else {
		portDBG_TRACE("TS controller regs: \n");
		logHexDump(regs, 19);
	}

	/*uint8_t eeprom[256];
	if((len = ts_readAllEeprom(eeprom, 256)) != 256)
	{
		portDBG_TRACE("Could not read complete TS EEPROM: %d", len);
		if(len > 0) {
			logHexDump(eeprom, len);
		}
	} else {
		portDBG_TRACE("TS controller EEPROM: \n");
		logHexDump(eeprom, 256);
	}*/

	ts_setTouchMode();
}

static void initQuadratureEncoder()
{
	gpio_configure_pin(AVR32_PIN_PB07, GPIO_DIR_OUTPUT);
	gpio_set_pin_low(AVR32_PIN_PB07);
	gpio_configure_pin(QEC_A, GPIO_DIR_INPUT);
	gpio_configure_pin(QEC_B, GPIO_DIR_INPUT);
	gpio_enable_pin_interrupt(QEC_A, GPIO_PIN_CHANGE);
	gpio_enable_pin_interrupt(QEC_B, GPIO_PIN_CHANGE);
	INTC_register_interrupt(qec_irq, QEC_PC_IRQ, 0);
	g_qec_state = (AVR32_GPIO.port[0].pvr & QEC_MASK)>>QEC_OFFSET;
	/*
	const eic_options_t options = {
		.eic_async = EIC_ASYNCH_MODE,
		.eic_edge = EIC_EDGE_FALLING_EDGE,
		.eic_filter = EIC_FILTER_ENABLED,
		.eic_level = EIC_LEVEL_LOW_LEVEL,
		.eic_line = QEC_A_EIC_INT,
		.eic_mode = EIC_EDGE_FALLING_EDGE,
	};
	eic_init(&AVR32_EIC, &options, 1);
	INTC_register_interrupt(qec_irq, QEC_A_IRQ, 0);
	eic_enable_line(&AVR32_EIC, QEC_A_EIC_INT);
	eic_enable_interrupt_line(&AVR32_EIC, QEC_A_EIC_INT);
	*/
}

static uint8_t getButtonState() 
{
	uint8_t data = 0;
	RESET_PIN(SW_LATCH);
	delay_ns(200);
	SET_PIN(SW_LATCH);
	delay_ns(200);
	for(int i = 0; i < 8; i++)
	{
		delay_ns(200);
		SET_PIN(SW_CLK);
		delay_ns(70);
		data = (data << 1) | GET_PIN(SW_DATA);
		delay_ns(70);
		RESET_PIN(SW_CLK);
	}
	return data;
}


void createUITask()
{	
	portDBG_TRACE("Creating task %s", "Input");
	g_ts_val_semaphore = xSemaphoreCreateMutexStatic(&g_ts_val_semaphore_buffer);
	g_userInput_taskHandle = xTaskCreateStatic(uiTask, "Input", 
		TASK_STACKSIZE_USERINPUT, NULL, TASK_PRIORITY_USERINPUT,
		g_userInput_stack, &g_userInput_task);
}

void uiTask(void *parameters)
{
	touch_message_t ts_msg;
	(void) parameters;
	portDBG_TRACE("Task started");
	setupPinMapping();
	initTouchScreen();
	initQuadratureEncoder();
	
	g_initialized = 1;
	TickType_t xLastWakeTime = xTaskGetTickCount();
	int prevQecPos = g_qec_steps;
	for(;;)
	{
		// Retrieve button state
		uint8_t buttons = getButtonState();
		uint32_t notifyValue = 0;
		if((buttons ^ g_buttonState) != 0) {
			g_buttonState = buttons;
			notifyValue |= TASK_NOTIFY_GRAPHICS_BUTTONS;
			logMsg("[Input] Buttons: %02x\r\n", buttons);
		}

		if(g_ts_cal_mode != 0) {
			if(xSemaphoreTake(g_ts_cmd_semaphore, 0))
			{
				int len = g_ts_rx_buffer[1] + 2;
				uint8_t response = g_ts_rx_buffer[2];
				if(g_ts_cmd_buffer_size != 0 || len != 4 || response != TS_RESPONSE_SUCCESS || g_ts_rx_buffer[3] != TS_CALIBRATE_MODE) {
					portDBG_TRACE("Error receiving TS calibration message. Len: %d, response: %02x, cmd: %02x", len, response, g_ts_rx_buffer[3]);
				} else {
					setNextTSCalState();
				}
			}
		} else {
			while(getTSMessageIdx(&ts_msg, 0))
			{
				logMsg("[Input] Touch msg: %d %d %d\r\n", ts_msg.pen, ts_msg.x, ts_msg.y);
				xSemaphoreTake(g_ts_val_semaphore, portMAX_DELAY);
				g_ts_x = ts_msg.x;
				g_ts_y = ts_msg.y;
				g_ts_pressed = ts_msg.pen;
				xSemaphoreGive(g_ts_val_semaphore);
				xTaskNotify(g_graphics_taskHandle, TASK_NOTIFY_GRAPHICS_TOUCH, eSetBits);
			}
		}
		
		if(g_qec_steps != prevQecPos) {
			int newPos = g_qec_steps/4;
			g_qec_dpos = newPos - g_qec_pos;
			g_qec_pos = newPos;
			prevQecPos = g_qec_steps;
			notifyValue |= TASK_NOTIFY_GRAPHICS_QEC;
			logMsg("[Input] QEC pos: %d %d %x\n\r", g_qec_pos, g_qec_dpos);
		}
		if(notifyValue) {
			xTaskNotify(g_graphics_taskHandle, notifyValue, eSetBits);
		}

		vTaskDelayUntil(&xLastWakeTime, TASK_PERIOD_USERINPUT * portTICK_PERIOD_MS);
	}
	 
	vTaskDelete(NULL);
}

void startTSCalibration()
{
	while(!g_initialized);
	logMsg("[Input] Starting calibration\n\r");
	ts_startCalibration();
	g_ts_cal_mode = 1;
	setNextUIState(STATE_cal_step1);
}