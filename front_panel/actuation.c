/*
 * actuation.c
 *
 * Created: 19-3-2017 11:42:13
 *  Author: Robojan
 */ 

#include <FreeRTOS.h>
#include <task.h>
#include <gpio.h>
#include <pwm.h>
#include <twi.h>

#include "tasks.h"
#include "settings.h"

TaskHandle_t g_actuation_taskHandle;
static StaticTask_t g_actuation_task;
static StackType_t g_actuation_stack[TASK_STACKSIZE_ACTUATION];

static const uint16_t ledBrightnessCurve[101] = {
	0,3,6,9,12,15,18,21,25,28,31,35,
	39,42,46,50,54,58,62,66,70,75,79,
	84,89,93,98,103,109,114,119,125,
	130,136,142,148,154,160,167,173,
	180,187,194,201,208,216,223,231,
	239,247,255,264,273,282,291,300,
	310,319,329,339,350,360,371,382,
	394,405,417,429,442,454,467,480,
	494,508,522,536,551,566,582,597,
	613,630,647,664,682,700,718,737,
	756,776,796,816,837,859,881,903,
	926,950,973,998,1023
};

static const uint8_t prepotAddresses[2] = {I2C_CHA_PREPOT_ADDR, I2C_CHB_PREPOT_ADDR};
static const uint8_t daciAddresses[2] = {I2C_CHA_DACI_ADDR, I2C_CHB_DACI_ADDR};
static const uint8_t dacvAddresses[2] = {I2C_CHA_DACV_ADDR, I2C_CHB_DACV_ADDR};
static const int outEnPins[2] = {OUTPUT_ENABLE_CHA, OUTPUT_ENABLE_CHB};

static uint8_t g_fan_val = 0;
static uint16_t g_bl_val = 0;
static uint16_t g_prepot_lowVal[2] = {0,0};
static uint16_t g_prepot_val[2] = {0,0};
static uint16_t g_daci_val[2] = {0,0};
static uint16_t g_dacv_val[2] = {0,0};
static uint8_t g_out_enable[2] = {0,0};

void setOutputEnabled(int channel, int enabled)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	g_out_enable[channel] = enabled != 0;
}

int getOutputEnabled(int channel)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	return g_out_enable[channel];
}

void EmergencyShutdown()
{
	setOutputEnabled(0,0);
	setOutputEnabled(1,0);
}

void setBacklight(int brightness) 
{
	if(brightness > 100) {
		brightness = 100; 
	}
	if(brightness < 0) {
		brightness = 0;
	}
	g_bl_val = ledBrightnessCurve[brightness];
}

void setFanSpeed(int speed) 
{
	if(speed < 0) speed = 0;
	if(speed > 100) speed = 100;
	if(speed != 0) {
		speed = 20 + (speed * 235) / 100;
	}
	g_fan_val = speed;
}

int getFanSpeed()
{
	int speed = g_fan_val - 20;
	speed = (speed * 100)/235;
	if(speed < 0) speed = 0;
	if(speed > 100) speed = 100;
	return speed;
}

uint16_t getRawValueFromCurrent(int current)
{
	if(current > 1463) current = 1463;
	if(current < 0) current = 0;
	return (current * 1146740)/409600;
}

int getCurrentFromRawValue(int value)
{
	if(value > 4095) value = 4095;
	if(value < 0) value = 0;
	return (value * 409600) / 1146740;
}

uint16_t getRawValueFromVoltage(int voltage)
{
	if(voltage > 3000) voltage = 3000;
	if(voltage < 0) voltage = 0;
	return (voltage * 681135) / 498961;
}

int getVoltageFromRawValue(int value)
{
	if(value > 4095) value = 4095;
	if(value < 0) value = 0;
	return (value * 498961) / 681135;
}

void setChannelRawVoltage(int channel, uint16_t value)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	logMsg("Setting raw voltage on channel %d to %d\n", channel, value);
	if(value > 4095) value = 4095;
	g_dacv_val[channel] = value;
}

void setChannelVoltage(int channel, int voltage)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	//int value = (voltage * 4095f * 499.0f * 5)/((499.0f + 6810.0f) * 2.048f * 100 * 5);
	if(voltage > 3000) voltage = 3000;
	if(voltage < 0) voltage = 0;
	int value = (voltage * 681135)/498961;
	g_dacv_val[channel] = value;
}

int getChannelVoltage(int channel)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	return (g_dacv_val[channel] * 498961 + 681135/2)/681135;
}

void setChannelRawCurrent(int channel, uint16_t value)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	if(value > 4095) value = 4095;
	logMsg("Setting raw current on channel %d to %d\n", channel, value);
	g_daci_val[channel] = value;
}

void setChannelCurrent(int channel, int current)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	//float vout = current * 0.05f * 5600.0f * 4095.5f/(200.0f * 1000.0f * 2.048f);
	if(current > 1463) current = 1463;
	if(current < 0) current = 0;
	int vout = (current * 1146740)/409600;
	g_daci_val[channel] = vout;
}

int getChannelCurrent(int channel)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	return (g_daci_val[channel] * 409600 + 1146740/2) / 1146740;
}

void setChannelPreVoltage(int channel, int voltage)
{
	log_assert(channel <= 1 && channel >= 0, "Channel > 1  || channel < 0");
	// R1 = 100k
	// R2 = 20k
	// R3 = 4.7k
	// Vref = 1.229
	//      (Vref * R1 * R3 + Vref * R1 * R2 + Vref * R2 * R3 - voltage/100 * R2 * R3) 
	// R4 = ----------------------------------------------------------------------
	//		(voltage/100 * R2 - Vref * R1 - Vref * R2)
	//      (Vref * (R1 * R3 + R1 * R2 + R2 * R3)/1000 - voltage/100 * (R2 * R3)/1000)
	// R4 = ----------------------------------------------------------------------
	//		(voltage/100 * R2/1000 - Vref * (R1 + R2)/1000)
	//      (Vref * (R1 * R3 + R1 * R2 + R2 * R3)/1000 - voltage/100 * (R2 * R3)/1000)
	// R4 = ----------------------------------------------------------------------
	//		(voltage/100 * R2/1000 - Vref * (R1 + R2)/1000)
	//      (3151156 - voltage * 940)
	// R4 = ----------------------------------------------------------------------
	//		((voltage * 200 - 147480)/1000)
	if(voltage < 1576) voltage = 1576;
	if(voltage > 3350) voltage = 3350;
	int R4 = 10000-(3151156-voltage*940)/((voltage*200-147480)/1000);
	int value = (256*R4+5000)/10000;
	log_assert(value <= 256 && value >= 0, "value > 0  && value <= 256");
	g_prepot_val[channel] = value;
}

int getChannelPreVoltage(int channel)
{
	log_assert(channel <= 1 && channel >= 0, "Channel <= 1  && channel >= 0");
	int Rx = 10000-((g_prepot_val[channel] * 10000 - 5000) / 256) + 4700;
	int R = 20000*Rx/(20000+Rx);
	int voltage = 1229*(100000+R)/(R*10);
	return voltage;
}

void setPowerLimitVoltage(int channel, int voltage)
{
	log_assert(channel <= 1 && channel >= 0, "Channel <= 1  && channel >= 0");
	if(voltage < 1576) voltage = 1576;
	if(voltage > 3350) voltage = 3350;
	int R4 = 10000-(3151156-voltage*940)/((voltage*200-147480)/1000);
	int value = (256*R4+5000)/10000;
	log_assert(value <= 256 && value >= 0, "value > 0  && value <= 256");
	g_prepot_lowVal[channel] = value;
}

static void actuationSysInit() 
{
	gpio_enable_module_pin(BL, BL_FUNC);
	gpio_enable_module_pin(FAN, FAN_FUNC);
	
	pwm_opt_t pwm_opts;
	pwm_opts.diva = 0;
	pwm_opts.prea = 0;
	pwm_opts.divb = 0;
	pwm_opts.preb = 0;

	pwm_init(&pwm_opts);
	avr32_pwm_channel_t bl_opts;
	bl_opts.cmr = 0x206;
	bl_opts.cprd = 0x400;
	bl_opts.cdty = 0x00;
	pwm_channel_init(BL_PWM, &bl_opts);
	avr32_pwm_channel_t fan_opts;
	fan_opts.cmr = 0x203;
	fan_opts.cprd = 0x100;
	fan_opts.cdty = 0x00;
	pwm_channel_init(FAN_PWM, &fan_opts);

	pwm_start_channels(BL_MASK | FAN_MASK);
	
	gpio_configure_pin(OUTPUT_ENABLE_CHA, GPIO_DIR_OUTPUT);
	gpio_set_pin_low(OUTPUT_ENABLE_CHA);
	gpio_configure_pin(OUTPUT_ENABLE_CHB, GPIO_DIR_OUTPUT);
	gpio_set_pin_low(OUTPUT_ENABLE_CHB);
}

static uint16_t readMCP4551TCON(int ch) {
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].prepot) return 0xFFFF;
	uint16_t data;
	twi_package_t package;
	package.addr[0] = 0x4C;
	package.addr_length = 1;
	package.buffer = &data;
	package.length = 2;
	package.chip = prepotAddresses[ch];
	int status;
	if((status = twi_master_read(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error reading from MCP4551 Channel %d: %d\n", ch, status);
		return 0;
	}
	return data;
}

static uint16_t readMCP4551Wiper(int ch) {
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].prepot) return 0;
	uint16_t data;
	twi_package_t package;
	package.addr[0] = 0x0C;
	package.addr_length = 1;
	package.buffer = &data;
	package.length = 2;
	package.chip = prepotAddresses[ch];
	int status;
	if((status = twi_master_read(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error reading from MCP4551 Channel: %d\n", ch, status);
		return 0;
	}
	return data;
}

static void writeMCP4551Wiper(int ch, uint16_t val) {
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].prepot) return;
	uint8_t data[2];
	data[0] = ((val >> 8) & 0x3);
	data[1] = val & 0xFF;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = data;
	package.length = 2;
	package.chip = prepotAddresses[ch];
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error writing to MCP4551 Channel %d: %d\n", ch, status);
	}
}

static void writeMCP4551TCON(int ch, uint16_t tcon) {
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].prepot) return;
	uint8_t data[2];
	data[0] = 0x40 | ((tcon >> 8) & 0x3);
	data[1] = tcon & 0xFF;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = data;
	package.length = 2;
	package.chip = prepotAddresses[ch];
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error writing to MCP4551 Channel %d TCON: %d\n", ch, status);
	}
}

static void writeDACI(int ch, uint16_t value)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].daci) return;
	value = value & 0x0FFF;
	uint8_t data[2];
	data[0] =((value >> 8) & 0xFF);
	data[1] = value & 0xFF;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = data;
	package.length = 2;
	package.chip = daciAddresses[ch];
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error writing to current DAC121c081 channel %d: %d\n", ch, status);
	}
}

static void writeDACV(int ch, uint16_t value) 
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].dacv) return;
	value = value & 0x0FFF;
	uint8_t data[2];
	data[0] =((value >> 8) & 0xFF);
	data[1] = value & 0xFF;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = data;
	package.length = 2;
	package.chip = dacvAddresses[ch];
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Act] Error writing to voltage DAC121c081 channel %d: %d\n", ch, status);
	}
}

void createActuationTask()
{
	portDBG_TRACE("Creating task %s", "Actuation");
	g_actuation_taskHandle = xTaskCreateStatic(actuationTask, "Actuation",
		TASK_STACKSIZE_ACTUATION, NULL, TASK_PRIORITY_ACTUATION,
		g_actuation_stack, &g_actuation_task);
}

static void initPreReg(int ch) 
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	logMsg("[Act] MCP4551 channel %d Wiper val: %d\n", ch, readMCP4551Wiper(ch));
	writeMCP4551Wiper(ch, g_prepot_val[ch]);
	logMsg("[Act] MCP4551 channel %d TCON: %02X\n", ch, readMCP4551TCON(ch));
	writeMCP4551TCON(ch, 0x0FF);
	logMsg("[Act] MCP4551 channel %d TCON: %02X\n", ch, readMCP4551TCON(ch));
}

static void initDAC(int ch) 
{
	writeDACI(ch, 0);
	writeDACV(ch, 0);
}

static void ProbeAllI2CDevices() {
	for(uint8_t addr = 0; addr <= 127; addr++) {
		if(twi_probe(&I2C, addr) == 0) {
			logMsg("I2C device found on %02x\n", addr);
		} else {
			logMsg("No device found on %02x\n", addr);
		}
	}
}

void actuationTask(void *parameters)
{
	(void)parameters;
	portDBG_TRACE("Task started");

	actuationSysInit();
	
	//ProbeAllI2CDevices();
	
	for(int i = 0; i < 2; i++){
		initPreReg(i);
		initDAC(i);
		int voltage = GetBootVoltage(i);
		int current = GetBootCurrent(i);
		setChannelPreVoltage(i, voltage + 300);
		setChannelRawVoltage(i, GetCalibratedDACVoltage(i, voltage));
		setChannelRawCurrent(i, GetCalibratedDACCurrent(i, current));
	}

	uint16_t lastPrePot[2] = {0,0};
	uint16_t lastDacV[2] = {0,0};
	uint16_t lastDacI[2] = {0,0};
	TickType_t xLastWakeTime = xTaskGetTickCount();
	int i = 0;
	for(;;) 
	{
		for(int i = 0; i < 2; i++) {
			int prepotVal = g_prepot_val[i] < g_prepot_lowVal[i] ? g_prepot_val[i] : g_prepot_lowVal[i];
			if(lastPrePot[i] != prepotVal) {
				writeMCP4551Wiper(i, prepotVal);
				lastPrePot[i] = prepotVal;
			}
			if(lastDacI[i] != g_daci_val[i]) {
				writeDACI(i, g_daci_val[i]);
				lastDacI[i] = g_daci_val[i];
			}
			if(lastDacV[i] != g_dacv_val[i]) {
				writeDACV(i, g_dacv_val[i]);
				lastDacV[i] = g_dacv_val[i];
			}
			if(g_out_enable[i]) {
				gpio_set_pin_high(outEnPins[i]);
			} else {
				gpio_set_pin_low(outEnPins[i]);
			}
		}
		AVR32_PWM.channel[BL_PWM].cupd = g_bl_val;
		AVR32_PWM.channel[FAN_PWM].cupd = g_fan_val;
		vTaskDelayUntil(&xLastWakeTime, TASK_PERIOD_ACTUATION * portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}
