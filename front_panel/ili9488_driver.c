/*
 * ili9488_driver.c
 *
 * Created: 18-2-2017 02:30:38
 *  Author: Robojan
 */ 

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include "ili9488_driver.h"
#include <emgl/debug.h>
#include "ili9488_interface.h"

typedef int lcd_api_t;

static lcd_api_t g_api;
static emgl_coord_t lcd_width = 480;
static emgl_coord_t lcd_height = 320;


static void lcd_getSize(lcd_api_t *api, emgl_coord_t *width, emgl_coord_t *height)
{
	(void) api;
	*width = lcd_width;
	*height = lcd_height;
}

static void lcd_poll(lcd_api_t *api)
{
	(void) api;
}

static emgl_U32 readID()
{
	emgl_U16 id[3];
	lcd_readCmd(0xDA, 1, &id[0]);
	lcd_readCmd(0xDB, 1, &id[1]);
	lcd_readCmd(0xDC, 1, &id[2]);
	return (id[0] & 0xFF) | ((id[1] & 0xFF)<<8) | ((id[2] & 0xFF)<<16);
}

static emgl_U32 readIDIF()
{
	emgl_U16 id[3];
	lcd_readCmd(0x04, 3, id);
	return (id[0] & 0xFF) | ((id[1] & 0xFF)<<8) | ((id[2] & 0xFF)<<16);
}

static emgl_U32 readDisplayStatus()
{
	emgl_U16 status[4];
	lcd_readCmd(0x09, 4, status);
	return  (status[3] & 0xFF) | ((status[2] & 0xFF)<<8) | 
		((status[1] & 0xFF)<<16) | ((status[0] & 0xFF)<<24);
}

static void sleepIn()
{
	lcd_writeCmd(0x10, 0, NULL);
}

static void sleepOut()
{
	lcd_writeCmd(0x11, 0, NULL);
	vTaskDelay(120*portTICK_PERIOD_MS);
}

static void partialModeOn() 
{
	lcd_writeCmd(0x12, 0, NULL);
}

static void normalModeOn() 
{
	lcd_writeCmd(0x13, 0, NULL);
}

static void displayInvOff()
{
	lcd_writeCmd(0x20, 0, NULL);
}

static void displayInvOn()
{
	lcd_writeCmd(0x21, 0, NULL);
}

static void allPixelsOff()
{
	lcd_writeCmd(0x22, 0, NULL);
}

static void allPixelsOn()
{
	lcd_writeCmd(0x23, 0, NULL);
}

static void displayOff()
{
	lcd_writeCmd(0x28, 0, NULL);
}

static void displayOn()
{
	lcd_writeCmd(0x29, 0, NULL);
}

static void columnAddressSet(emgl_U16 start, emgl_U16 end)
{
	emgl_U16 params[4];
	params[0] = (start >> 8) & 0xFF;
	params[1] = (start >> 0) & 0xFF;
	params[2] = (end >> 8) & 0xFF;
	params[3] = (end >> 0) & 0xFF;
	lcd_writeCmd(0x2A, 4, params);
}

static void pageAddressSet(emgl_U16 start, emgl_U16 end)
{
	emgl_U16 params[4];
	params[0] = (start >> 8) & 0xFF;
	params[1] = (start >> 0) & 0xFF;
	params[2] = (end >> 8) & 0xFF;
	params[3] = (end >> 0) & 0xFF;
	lcd_writeCmd(0x2B, 4, params);
}

static void ramWrite(emgl_U32 n, const emgl_U16 *data)
{
	lcd_writeCmd(0x2C, n, data);
}

static void ramWriteRepeat(emgl_U32 n, emgl_U32 data)
{
	lcd_fillWrite(0x2C, data, n);
}

static void ramRead(emgl_U32 n, emgl_U16 *data)
{
	lcd_readCmd(0x2E, n, data);
}

static void setMemoryAccessControl(emgl_U8 madctl)
{
	emgl_U16 param = madctl;
	lcd_writeCmd(0x36,1, &param);
}

static void idleModeOff()
{
	lcd_writeCmd(0x38, 0, NULL);
}

static void idleModeOn()
{
	lcd_writeCmd(0x39, 0, NULL);
}

static void setInterfacePixelFormat(emgl_U8 colmod)
{
	emgl_U16 param = colmod;
	lcd_writeCmd(0x3A, 1, &param);
}

static emgl_U16 readScanLine() 
{
	emgl_U16 data[2];
	lcd_readCmd(0x45, 2, data);
	return (data[1] & 0xFF) | ((data[0] & 0xFF) << 8);
}

static void setFramerate(emgl_U8 p1, emgl_U8 p2)
{
	emgl_U16 data[2];
	data[0] = p1;
	data[1] = p2;
	lcd_writeCmd(0xb1, 2, data);
}

static void setInversionControl(emgl_U8 invtr)
{
	emgl_U16 data;
	data = invtr;
	lcd_writeCmd(0xb4, 1, &data);
}

static void setBlankingPorch(emgl_U8 vfp, emgl_U8 vbp, emgl_U8 hfp, emgl_U8 hbp)
{
	emgl_U16 data[4];
	data[0] = vfp;
	data[1] = vbp;
	data[2] = hfp;
	data[3] = hbp;
	lcd_writeCmd(0xb5, 4, data);
}

static void setDisplayFunctionControl(emgl_U8 p1, emgl_U8 p2, emgl_U8 p3)
{
	emgl_U16 data[3];
	data[0] = p1;
	data[1] = p2;
	data[2] = p3;
	lcd_writeCmd(0xb6, 3, data);
}

static void setEntryModeSet(emgl_U8 etmod)
{
	emgl_U16 data;
	data = etmod;
	lcd_writeCmd(0xb7, 1, &data);
}

static void setPowerControl1(emgl_U8 vrh1, emgl_U8 vrh2)
{
	emgl_U16 data[2];
	data[0] = vrh1;
	data[1] = vrh2;
	lcd_writeCmd(0xc0, 2, data);
}

static void setPowerControl2(emgl_U8 pwctrl2)
{
	emgl_U16 data;
	data = pwctrl2;
	lcd_writeCmd(0xc1, 1, &data);
}

static void setPowerControl3(emgl_U8 pwctrl3)
{
	emgl_U16 data;
	data = pwctrl3;
	lcd_writeCmd(0xc2, 1, &data);
}

static void setPowerControl4(emgl_U8 pwctrl4)
{
	emgl_U16 data;
	data = pwctrl4;
	lcd_writeCmd(0xc3, 1, &data);
}

static void setPowerControl5(emgl_U8 pwctrl5)
{
	emgl_U16 data;
	data = pwctrl5;
	lcd_writeCmd(0xc4, 1, &data);
}

static void setVCOMControl(emgl_U8 p1, emgl_U8 p2)
{
	emgl_U16 data[4];
	data[0] = 0x00;
	data[1] = p1;
	data[2] = p2;
	data[3] = 0x00;
	lcd_writeCmd(0xc5, 4, data);
}

static void setImageFunction(emgl_U8 setimage)
{
	emgl_U16 data;
	data = setimage;
	lcd_writeCmd(0xE9, 1, &data);
}

static void setBrightness(emgl_U8 brightness)
{
	emgl_U16 data;
	data = brightness;
	lcd_writeCmd(0x51, 1, &data);
}

static void setAdjustControl3(emgl_U8 adjust3)
{
	emgl_U16 data[4];
	data[0] = 0xA9;
	data[1] = 0x51;
	data[2] = 0x2C;
	data[3] = adjust3;
	lcd_writeCmd(0xF7, 4, data);
}

static void setVerticalScrollingDef(emgl_U16 tfa, emgl_U16 vsa, emgl_U16 bfa)
{
	emgl_U16 data[6];
	data[0] = (tfa >> 8) & 0xFF;
	data[1] = (tfa >> 0) & 0xFF;
	data[2] = (vsa >> 8) & 0xFF;
	data[3] = (vsa >> 0) & 0xFF;
	data[4] = (bfa >> 8) & 0xFF;
	data[5] = (bfa >> 0) & 0xFF;
	lcd_writeCmd(0x33, 6, data);
}

static void lcd_init(lcd_api_t *api)
{
	(void) api;
	static const emgl_U16 pgamma[15] = {0x00, 0x03, 0x09, 0x08, 0x16, 0x0a, 0x3f, 0x78, 0x4c, 0x09, 0x0a, 0x08, 0x16, 0x1A, 0x0F};
	static const emgl_U16 ngamma[15] = {0x00, 0x16, 0x19, 0x03, 0x0F, 0x05, 0x32, 0x45, 0x46, 0x04, 0x0E, 0x0D, 0x35, 0x37, 0x0F};
	lcd_reset();

	emgl_U32 id = readID();
	EMGL_LOG(EMGL_LOGLVL_DBG | EMGL_LOGMOD_DRIVER, "LCD driver ID: %06X\n", id);
	if(id != 0x668054) {
		EMGL_LOG(EMGL_LOGLVL_ERR | EMGL_LOGMOD_DRIVER, "Incorrect LCD ID: %06x\n", id);
		return;
	}
	EMGL_LOG(EMGL_LOGLVL_DBG | EMGL_LOGMOD_DRIVER, "Display status: %08x\n", readDisplayStatus());
	EMGL_LOG(EMGL_LOGLVL_INFO | EMGL_LOGMOD_DRIVER, "LCD Initializing\n");
	setFramerate(0xa1, 0x11);
	setInversionControl(0x02);
	setBlankingPorch(2, 2, 10, 4);
	setDisplayFunctionControl(0x02, 0x02, lcd_width/8-1);
	setEntryModeSet(0xC6);
	setPowerControl1(0x17, 0x15);
	setPowerControl2(0x41);
	setPowerControl3(0x33);
	setPowerControl4(0x00);
	setPowerControl5(0x33);
	setVCOMControl(0x12, 0x80);
	setInterfacePixelFormat(0x55);
	lcd_writeCmd(0xE0, 15, pgamma);
	lcd_writeCmd(0xE1, 15, ngamma);
	setImageFunction(0x00);
	setBrightness(0xFF);
	setAdjustControl3(0x82);
	setMemoryAccessControl(0x68);
	setVerticalScrollingDef(0, lcd_height, 0);
	partialModeOn();
	sleepOut();
	displayOn();
	EMGL_LOG(EMGL_LOGLVL_DBG | EMGL_LOGMOD_DRIVER, "Display status: %08x\n", readDisplayStatus());
}

static void lcd_setWindow(emgl_coord_t x1, emgl_coord_t y1, emgl_coord_t x2, emgl_coord_t y2)
{
	columnAddressSet(x1, x2);
	pageAddressSet(y1, y2);
}

static emgl_color_t lcd_getPixel(void *api, emgl_coord_t x, emgl_coord_t y)
{
	lcd_setWindow(x, y, x+1, y+1);
	emgl_U16 color;
	ramRead(1, &color);
	return emgl_colorConvFromRGB565(color);
}

static void lcd_setPixel(void *api, emgl_coord_t x, emgl_coord_t y, 
	emgl_color_t color)
{
	lcd_setWindow(x, y, x+1, y+1);
	ramWrite(1, &color);
}

static void swapIfBigger(emgl_coord_t *a, emgl_coord_t *b)
{
	if(*a > *b) {
		emgl_coord_t temp = *b;
		*b = *a;
		*a = temp;
	}
}

static void lcd_fillRect(void *api, emgl_coord_t x1, emgl_coord_t y1, 
	emgl_coord_t x2, emgl_coord_t y2, emgl_color_t color)
{
	EMGL_ASSERT("x1 != x2", x1 != x2);
	EMGL_ASSERT("y1 != y2", y1 != y2);
	swapIfBigger(&x1, &x2);
	swapIfBigger(&y1, &y2);
	lcd_setWindow(x1, y1, x2, y2);
	emgl_coord_t sizex = x2 - x1 + 1;
	emgl_coord_t sizey = y2 - y1 + 1;
	ramWriteRepeat(sizex * sizey, emgl_colorConvToRGB565(color));
}

static void lcd_drawLineH(void *api, emgl_coord_t x1, emgl_coord_t x2, 
	emgl_coord_t y, emgl_color_t color) 
{
	if(x1 == x2) {
		lcd_setPixel(api, x1, y, color);
		return;
	}
	swapIfBigger(&x1, &x2);
	lcd_setWindow(x1, y, x2, y);
	ramWriteRepeat(x2 - x1 + 1, emgl_colorConvToRGB565(color));
}

static void lcd_drawLineV(void *api, emgl_coord_t y1, emgl_coord_t y2,
	emgl_coord_t x, emgl_color_t color)
{
	if(y1 == y2) {
		lcd_setPixel(api, x, y1, color);
		return;
	}
	swapIfBigger(&y1, &y2);
	lcd_setWindow(x, y1, x, y2);
	ramWriteRepeat(y2 - y1 + 1, emgl_colorConvToRGB565(color));
}

static void lcd_drawBitmap(void *api, emgl_coord_t x, emgl_coord_t y, emgl_coord_t width,
	emgl_coord_t height, const emgl_color_t *data)
{
	EMGL_ASSERT("lcd_drawBitmap: width >= 0 && height >= 0", width >= 0 && height >= 0);
	EMGL_ASSERT("emgl_color_t must be of type RGB565", EMGL_INTERNALCOLORMODE == CM_RGB565);
	emgl_coord_t startx = x;
	emgl_coord_t starty = y;
	emgl_coord_t endx = x + width;
	emgl_coord_t endy = y + height;

	if (x < 0)
	{
		startx = -x;
	}
	if (y < 0)
	{
		starty = -y;
	}

	lcd_setWindow(startx, starty, endx - 1, endy - 1);
	ramWrite(width * height, data);
}

static const emgl_driverAPI_t g_driver = {
	.api = (void*)&g_api,
	.poll = (void(*)(void*))lcd_poll,
	.getPixel = lcd_getPixel,
	.setPixel = lcd_setPixel,
	.fillRect = lcd_fillRect,
	.drawLineH = lcd_drawLineH,
	.drawLineV = lcd_drawLineV,
	.drawBitmap = lcd_drawBitmap,
	.init = (void (*)(void*))lcd_init,
	.getSize = (void (*)(void*, emgl_coord_t *, emgl_coord_t *))lcd_getSize,
};
 
const emgl_driverAPI_t *lcd_getDriver() {
	return &g_driver;
}

