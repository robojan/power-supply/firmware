/*
 * main.c
 *
 * Created: 24-2-2017 12:09:01
 *  Author: Robojan
 */ 

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <pm.h>
#include <flashc.h>
#include <sleep.h>
#include <twi.h>
#include <gpio.h>

#include "tasks.h"
#include "settings.h"
#include "log.h"

// Static task memory
static StaticTask_t xIdleTaskTCB;
static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];
static StaticTask_t xTimerTaskTCB;
static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

system_status_t g_status;

static void initI2C()
{
	gpio_enable_module_pin(I2C_SCL, I2C_SCL_FUNC);
	gpio_enable_module_pin(I2C_SDA, I2C_SDA_FUNC);

	twi_options_t opts;
	opts.chip = 0x00;
	opts.speed = 400000;
	opts.pba_hz = FPBA;
	twi_master_init(&I2C, &opts);
}

static bool testADC(uint8_t addr) {
	uint16_t tmp;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = (uint8_t *)&tmp;
	package.length = 2;
	package.chip = addr;
	return twi_master_read(&I2C, &package) == TWI_SUCCESS;
}

static bool testDAC(uint8_t addr) {
	uint16_t tmp;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = (uint8_t *)&tmp;
	package.length = 2;
	package.chip = addr;
	return twi_master_read(&I2C, &package) == TWI_SUCCESS;
}

static bool testPrePot(uint8_t addr) {
	uint16_t tmp;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = (uint8_t *)&tmp;
	package.length = 2;
	package.chip = addr;
	return twi_master_read(&I2C, &package) == TWI_SUCCESS;
}

static void selfTest()
{
	// TODO temp test
	g_status.temp = 1;
	g_status.ch[0].adc = testADC(I2C_CHA_ADC_ADDR);
	if(!g_status.ch[0].adc) logMsg("[Diag] CHA ADC does not respond\n");
	g_status.ch[0].daci = testDAC(I2C_CHA_DACI_ADDR);
	if(!g_status.ch[0].daci) logMsg("[Diag] CHA current set DAC does not respond\n");
	g_status.ch[0].dacv = testDAC(I2C_CHA_DACV_ADDR);
	if(!g_status.ch[0].dacv) logMsg("[Diag] CHA voltage set DAC does not respond\n");
	g_status.ch[0].prepot = testPrePot(I2C_CHA_PREPOT_ADDR);
	if(!g_status.ch[0].prepot) logMsg("[Diag] CHA preregulation pot does not respond\n");
	g_status.ch[1].adc = testADC(I2C_CHB_ADC_ADDR);
	if(!g_status.ch[1].adc) logMsg("[Diag] CHB ADC does not respond\n");
	g_status.ch[1].daci = testDAC(I2C_CHB_DACI_ADDR);
	if(!g_status.ch[1].daci) logMsg("[Diag] CHB current set DAC does not respond\n");
	g_status.ch[1].dacv = testDAC(I2C_CHB_DACV_ADDR);
	if(!g_status.ch[1].dacv) logMsg("[Diag] CHB voltage set DAC does not respond\n");
	g_status.ch[1].prepot = testPrePot(I2C_CHB_PREPOT_ADDR);
	if(!g_status.ch[1].prepot) logMsg("[Diag] CHB preregulation pot does not respond\n");
}

int main(void)
{
	
	portDBG_TRACE("Power supply booting up");
	portDBG_TRACE("Created by Robbert-Jan de Jager");

	initI2C();

	ENABLE_ALL_INTERRUPTS();

	portDBG_TRACE("Starting self-test");
	selfTest();
	
	portDBG_TRACE("Loading settings");
	LoadSettings();
	PrintSettings();

	portDBG_TRACE("Creating tasks");
	createGraphicsTask();
	createUITask();
	createActuationTask();
	createSensingTask();
	
	portDBG_TRACE("Starting scheduler");
	vTaskStartScheduler();
	
	portDBG_TRACE("Exiting main thread");
	while(1) {
		pm_sleep(AVR32_PM_SMODE_IDLE);
	}
	return 0;
}

void vApplicationIdleHook()
{
	if(eTaskGetState(g_graphics_taskHandle) == eReady || 
		eTaskGetState(g_actuation_taskHandle) == eReady || 
		eTaskGetState(g_sensing_taskHandle) == eReady || 
		eTaskGetState(g_userInput_taskHandle) == eReady) 
	{
		taskYIELD();
	}
}

void vApplicationMallocFailedHook()
{
	portDBG_TRACE("Malloc failed");
	while(1);
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, signed char *pcTaskName )
{
	(void) xTask;
	portDBG_TRACE("Stack overflow in task %s", pcTaskName);
	
	while(1);
}

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
	StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize)
{
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCB;
	*ppxIdleTaskStackBuffer = uxIdleTaskStack;
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, 
	StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize)
{
	*ppxTimerTaskTCBBuffer = &xTimerTaskTCB;
	*ppxTimerTaskStackBuffer = uxTimerTaskStack;
	*pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}