/*
 * log.h
 *
 * Created: 18-2-2017 01:35:14
 *  Author: Robojan
 */ 


#ifndef LOG_H_
#define LOG_H_

#ifdef __cplusplus
extern "C"{
#endif

void logMsg(const char *format, ...);
void logHexDump(const void *data, int len);
void log_fatal(const char *format, ...);
void log_assert(int cond, const char *msg);

#ifdef __cplusplus
}
#endif

#endif /* LOG_H_ */