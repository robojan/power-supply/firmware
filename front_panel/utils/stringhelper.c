/*
 * string.c
 *
 * Created: 02-09-17 17:47:12
 *  Author: Robojan
 */ 

#include "stringhelper.h"

char *getFixedPointNumberWithUnits(int value, int decimals, const char *unit, char *buffer, int bufferSize)
{
	char *ptr = buffer + bufferSize - 1;
	int neg = 0;
	if(value < 0) {
		neg = 1;
		value = -value;
	}
	*ptr-- = '\0';
	// Goto end of unit string
	const char *unitPtr = unit;
	while(*unitPtr != '\0') {
		unitPtr++;
	}
	// Copy the unit in the string
	while(unitPtr != unit) {
		unitPtr--;
		*ptr-- = *unitPtr;
	}
	// Print the decimals
	while(decimals-- > 0) {
		*ptr-- = '0' + (value % 10);
		value /= 10;
	}
	*ptr-- = '.';
	do {
		*ptr-- = '0' + (value % 10);
		value /= 10;
	} while(value != 0);
	if(neg != 0) {
		*ptr-- = '-';
	}
	return ptr + 1;
}