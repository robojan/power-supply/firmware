/*
 * string.h
 *
 * Created: 02-09-17 17:46:50
 *  Author: Robojan
 */ 


#ifndef UTILS_STRINGHELPER_H_
#define UTILS_STRINGHELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

char *getFixedPointNumberWithUnits(int value, int decimals, const char *unit, char *buffer, int bufferSize);

#ifdef __cplusplus
};
#endif

#endif /* UTILS_STRINGHELPER_H_ */