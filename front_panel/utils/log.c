/*
 * log.c
 *
 * Created: 18-2-2017 01:35:01
 *  Author: Robojan
 */ 

#include <stdarg.h>
#include <ctype.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>

static char *nano_itoa_uint_hex(char *buffer, int size, unsigned int x,
	const char *hexchars) {
	char *ptr = buffer + size - 1;
	char c = '\0';
	*ptr = c;
	do {
		c = hexchars[x % 16];
		x /= 16;
		ptr--;
		*ptr = c;
	} while( x != 0);
	return ptr;
}

static char *nano_itoa_uint_dec(char *buffer, int size, unsigned int x) {
	char *ptr = buffer + size - 1;
	char c = '\0';
	*ptr = c;
	do {
		c = '0' + x % 10;
		x /= 10;
		ptr--;
		*ptr = c;
	} while( x != 0);
	return ptr;
} 

static char *nano_itoa_int_dec(char *buffer, int size, int x) {
	if(x < 0) {
		x = -x;
		char *ptr = nano_itoa_uint_dec(buffer, size, (unsigned int) x);
		ptr--;
		*ptr = '-';
		return ptr;
	} else {
		return nano_itoa_uint_dec(buffer, size, x);
	}
}

struct printf_flags {
	int leftJustified : 1;
	int alwaysSign : 1;
	int spacePrepend : 1;
	int alternate : 1;
	int leadingZero : 1;
};

static void nano_print_unsigned(char *buffer, int buffersize, unsigned int value,
	struct printf_flags flags, int minWidth) {
	char tmp = '+';
	char *arg = nano_itoa_uint_dec(buffer, buffersize, value);
	int len = strlen(arg);
	if(flags.alwaysSign) {
		if(minWidth > 0) minWidth--;
		_write_r(_REENT, 0, &tmp, 1);
	}
	int width = minWidth - len;
	if(flags.leftJustified) {
		_write_r(_REENT, 0, arg, len);
		tmp = ' ';
		while(width > 0) {
			_write_r(_REENT, 0, &tmp, 1);
			width--;
		}
		return;
	}
	tmp = flags.leadingZero ? '0' : ' ';
	while(width > 0) {
		_write_r(_REENT, 0, &tmp, 1);
		width--;
	}
	_write_r(_REENT, 0, arg, len);
}

static void nano_print_integer(char *buffer, int buffersize, int value, 
	struct printf_flags flags, int minWidth) {
	char tmp = '+';
	char *arg = nano_itoa_int_dec(buffer, buffersize, value);
	int len = strlen(arg);
	if(flags.alwaysSign && value >= 0) {
		if(minWidth > 0) minWidth--;
		_write_r(_REENT, 0, &tmp, 1);
	}
	int width = minWidth - len;
	if(flags.leftJustified) {
		_write_r(_REENT, 0, arg, len);
		tmp = ' ';
		while(width > 0) {
			_write_r(_REENT,0, &tmp, 1);
			width--;
		}
		return;
	}
	tmp = flags.leadingZero ? '0' : ' ';
	while(width > 0) {
		_write_r(_REENT, 0, &tmp, 1);
		width--;
	}
	_write_r(_REENT, 0, arg, len);
}

static void nano_print_hex(char *buffer, int buffersize, unsigned int value,
	struct printf_flags flags, int minWidth, const char *hexchars) {
	char tmp = '+';
	char *arg = nano_itoa_uint_hex(buffer, buffersize, value, hexchars);
	int len = strlen(arg);
	int width = minWidth - len;
	if(flags.leftJustified) {
		_write_r(_REENT, 0, arg, len);
		tmp = ' ';
		while(width > 0) {
			_write_r(_REENT, 0, &tmp, 1);
			width--;
		}
		return;
	}
	tmp = flags.leadingZero ? '0' : ' ';
	while(width > 0) {
		_write_r(_REENT, 0, &tmp, 1);
		width--;
	}
	_write_r(_REENT, 0, arg, len);

}

static void nano_vprintf(const char *format, va_list args)
{
	static const char errorArg[] = "<err>";
	static const char hexChars[] = "0123456789abcdef";
	static const char hexCChars[] = "0123456789ABCDEF";
	static char buffer[16];
	struct printf_flags flags = {
		.leftJustified = false,
		.alwaysSign = false,
		.spacePrepend = false,
		.alternate = false,
		.leadingZero = false,
	};
	int minimumFieldWidth = 0;
	const char *start = format;
	const char *ptr = start;
	int i;
	// Search for normal printable part
	while(*ptr != '\0')
	{
		while(*ptr != '\0' && *ptr != '%') {
			ptr++;
		}
		_write_r(_REENT,0, start, ptr - start);
		start = ptr;
		if(*ptr == '\0') break;
		ptr++;
		// flags
		while(*ptr == '-' || *ptr == '+' || *ptr == ' ' || *ptr == '#' || *ptr == '0') {
			if(*ptr == '-') {
				flags.leftJustified = 1;
			}
			else if(*ptr == '+') {
				flags.alwaysSign = 1;
			}
			else if(*ptr == ' ') {
				flags.spacePrepend = 1;
			}
			else if(*ptr == '#') {
				flags.alternate = 1;
			}
			else if(*ptr == '0') {
				flags.leadingZero = 1;
			}
			ptr++;
		}
		if(flags.alwaysSign && flags.spacePrepend) flags.spacePrepend = false;
		if(flags.leadingZero && flags.leftJustified) flags.leadingZero = false;

		// minimum field width 
		if(*ptr == '*') {
			minimumFieldWidth = va_arg(args, int);
			if(minimumFieldWidth < 0) minimumFieldWidth = 0;
		} else if (isdigit(*ptr)) {
			// Maximum of 3 characters
			for(i = 0; i < 3 && isdigit(*ptr); i++, ptr++) {
				buffer[i] = *ptr;
			}
			buffer[i] = '\0';
			minimumFieldWidth = atoi(buffer);
		}

		// Precision, This is not supported
		if(*ptr == '.') {
			ptr++;
			if(*ptr == '*') {
				va_arg(args, int);
			} else {
				while(isdigit(*ptr)) {
					ptr++;
				}
			}
		}

		// length modifier
		if(*ptr == 'h') {
			ptr++;
			if(*ptr == 'd' || *ptr == 'i' || *ptr == 'x' || *ptr == 'X' || *ptr == 'u') {// short
				int value = va_arg(args, int);
				if(*ptr == 'd' || *ptr == 'i' || *ptr == 'u') {
					value = va_arg(args, int);
					nano_print_integer(buffer, sizeof(buffer), value, flags, 
						minimumFieldWidth);
					
				} else {
					if(*ptr == 'x') {
						nano_print_hex(buffer, sizeof(buffer), value, flags,
							minimumFieldWidth, hexCChars);
					} else {
						nano_print_hex(buffer, sizeof(buffer), value, flags,
							minimumFieldWidth, hexChars);
					}
				}
				ptr++;
			} else {
				_write_r(_REENT, 0, errorArg, sizeof(errorArg));
				ptr++;
				
			}
		} else if (*ptr == 'l') {
			ptr++;
			ptr++;
			_write_r(_REENT, 0, errorArg, sizeof(errorArg));
		} else {
			if(*ptr == 'd' || *ptr == 'i' || *ptr == 'x' || *ptr == 'X' || *ptr == 'u' || *ptr == 'p') {// short
				if(*ptr == 'd' || *ptr == 'i') {
					int value = va_arg(args, int);
					nano_print_integer(buffer, sizeof(buffer), value, flags,
						minimumFieldWidth);
				} else {
					unsigned int value = va_arg(args, unsigned int);
					if(*ptr == 'u') {
						nano_print_unsigned(buffer, sizeof(buffer), value, flags,
							minimumFieldWidth);
					} else {
						if(*ptr == 'x') {
							nano_print_hex(buffer, sizeof(buffer), value, flags,
								minimumFieldWidth, hexCChars);
						} else {
							nano_print_hex(buffer, sizeof(buffer), value, flags,
								minimumFieldWidth, hexChars);
						}
					}
				}
				ptr++;
			} else if (*ptr == 's') {
				ptr++;
				const char *value = va_arg(args, const char *);
				int len = strlen(value);
				int width = minimumFieldWidth - len;
				if(flags.leftJustified) {
					_write_r(_REENT, 0, value, len);
					buffer[0] = ' ';
					while(width > 0) {
						_write_r(_REENT, 0, &buffer[0], 1);
						width--;
					}
				} else {
					buffer[0] = ' ';
					while(width > 0) {
						_write_r(_REENT, 0, &buffer[0], 1);
						width--;
					}
					_write_r(_REENT, 0, value, len);
				}
			} else {
				_write_r(_REENT, 0, errorArg, sizeof(errorArg));
				ptr++;
			}
		}
		
		flags.leftJustified = false;
		flags.alwaysSign = false;
		flags.spacePrepend = false;
		flags.alternate = false;
		flags.leadingZero = false;
		minimumFieldWidth = 0;
		start = ptr;
	}
}

void logMsg(const char *format, ...)
{
	va_list args; 
	va_start(args, format);
	nano_vprintf(format, args);
	//vprintf(format, args);
	va_end(args);
}

void log_fatal(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	nano_vprintf(format, args);
	//vprintf(format, args);
	va_end(args);
	while(1==1) 
	{
		vTaskDelay(1000*portTICK_PERIOD_MS);
	}
}

void log_assert(int cond, const char *msg)
{
	if(!cond) {
		log_fatal(msg);
	}
}

void logHexDump(const void *data, int len)
{
	const char *ptr = (const char *)data;
	logMsg("Addr 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\n");
	int addr = 0;
	while(len > 16) {
		logMsg("%04x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
			addr, ptr[0], ptr[1], ptr[2], ptr[3], ptr[4],
			ptr[5], ptr[6], ptr[7], ptr[8],
			ptr[9], ptr[10], ptr[11], ptr[12],
			ptr[13], ptr[14], ptr[15]);
		len -= 16;
		addr += 16;
		ptr += 16;
	}
	logMsg("%04x ", addr);
	while(len > 0) {
		logMsg("%02x ", *ptr);
		ptr++;
		len--;
	}
	logMsg("\n");
}