/*
 * sensing.c
 *
 * Created: 1-4-2017 17:16:54
 *  Author: Robojan
 */ 
 
#include <FreeRTOS.h>
#include <task.h>
#include <gpio.h>
#include <twi.h>

#include "tasks.h"

TaskHandle_t g_sensing_taskHandle;
static StaticTask_t g_sensing_task;
static StackType_t g_sensing_stack[TASK_STACKSIZE_SENSING];

#define ADS1115_CONV     0
#define ADS1115_CONFIG   1
#define ADS1115_LOTHRESH 2
#define ADS1115_HITRRESH 3

#define ADS1115_BASE_CONFIG 0x05a3
#define ADS1115_START_CONV  0x8000
#define ADS1115_MEAS_V      0x4000
#define ADS1115_MEAS_I      0x5000
#define ADS1115_MEAS_DRVI   0x6000

int g_temperature = 200;
int g_dispCurrent[2] = {0,0};
int g_instVoltage[2] = {0,0};
int g_dispVoltage[2] = {0,0};
int g_decayVoltage[2] = {0,0};
uint8_t g_cc[2] = {0,0};

static const uint8_t adcAddressTable[2] = {I2C_CHA_ADC_ADDR, I2C_CHB_ADC_ADDR};

static const uint16_t lmt01LookupTable[21] = {
	26, 181, 338, 494, 651, 808,
	966, 1125, 1284, 1443, 1602, 1762,
	1923, 2084, 2245, 2407, 2569, 2731,
	2893, 3057, 3218
};

static int getTempLookupIdx(uint16_t pulseCount)
{
	int lo = 0;
	int hi = sizeof(lmt01LookupTable)/sizeof(uint16_t)-1;
	while(lo <= hi) {
		int m = (lo + hi)/2;
		if(lmt01LookupTable[m] < pulseCount) {
			lo = m + 1;
		} else if(lmt01LookupTable[m] > pulseCount) {
			hi = m - 1;
		} else {
			return m;
		}
	}
	return hi;
}

static int getTempLookupTemp(int idx)
{
	return -500 + 100 * idx;
}

void createSensingTask()
{
	portDBG_TRACE("Creating task %s", "Sensing");
	g_actuation_taskHandle = xTaskCreateStatic(sensingTask, "Sensing",
		TASK_STACKSIZE_SENSING, NULL, TASK_PRIORITY_SENSING,
		g_sensing_stack, &g_sensing_task);
}

static void tempTimerSync()
{
	bool inIdle = true;
	volatile avr32_tc_channel_t *timer = &AVR32_TC.channel[TEMP_TIMER];
	
	// Wait until not in idle
	while(inIdle) {
		timer->ccr = 0x4; // Reset Timer
		TickType_t startTime = xTaskGetTickCount();
		while(xTaskGetTickCount() - startTime == 0);
		inIdle = timer->cv == 0;
	}
	// Wait until in idle
	while(!inIdle) {
		timer->ccr = 0x4; // Reset Timer
		TickType_t startTime = xTaskGetTickCount();
		while(xTaskGetTickCount() - startTime == 0);
		inIdle = timer->cv == 0;
	}
}

static void initTempSensor()
{
	gpio_enable_module_pin(TEMP, TEMP_FUNC);
	volatile avr32_tc_channel_t *timer = &AVR32_TC.channel[TEMP_TIMER];
	timer->CCR.clkdis = 1; // Disable the clk
	timer->cmr = 0x00008008 | (TEMP_CLK+5); // set external clk input, increment on falling edge
	AVR32_TC.BMR.tc0xc0s = 0;
	timer->CCR.clken = 1; // Enable the clk

	tempTimerSync();
}

static void ADS1115_write_register(int ch, uint8_t reg, uint16_t val)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	
	uint8_t data[3];
	data[0] = reg;
	data[1] = (val >> 8) & 0xFF;
	data[2] = val & 0xFF;
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = data;
	package.length = 3;
	package.chip = adcAddressTable[ch];
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Sens] Error writing to ADS1115 Channel %d reg: %d\n", ch, status);
	}
}

static uint16_t ADS1115_read_register(int ch, uint8_t reg)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	uint8_t addr = adcAddressTable[ch];
	twi_package_t package;
	package.addr[0] = 0;
	package.addr_length = 0;
	package.buffer = &reg;
	package.length = 1;
	package.chip = addr;
	int status;
	if((status = twi_master_write(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Sens] Error writing to ADS1115 Channel %d APR: %d\n", addr, status);
		return 0;
	}
	uint8_t data[2];
	package.buffer = &data;
	package.length = 2;
	package.chip = addr;
	if((status = twi_master_read(&I2C, &package)) != TWI_SUCCESS) {
		logMsg("[Sens] Error reading from ADS1115 Channel %d reg: %d\n", addr, status);
		return 0;
	}
	return (data[0] << 8) | data[1];
}

static int16_t ADC_getResult(int ch)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].adc) return 0x8000;
	
	return (int16_t)ADS1115_read_register(ch, ADS1115_CONV);
}

static void ADC_startConversion(int ch, uint16_t measurement)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].adc) return;
	ADS1115_write_register(ch, ADS1115_CONFIG, ADS1115_BASE_CONFIG | measurement | ADS1115_START_CONV);
}

static void initADC(int ch)
{
	log_assert(ch <= 1 && ch >= 0, "Channel > 1  || channel < 0");
	if(!g_status.ch[ch].adc) return;
	ADS1115_write_register(ch, ADS1115_CONFIG, ADS1115_BASE_CONFIG);
	uint16_t config = ADS1115_read_register(ch, ADS1115_CONFIG);
	logMsg("[Sens] ADS1115 channel %d config: %04x\n", ch, config);


	ADC_startConversion(ch, ADS1115_MEAS_V);
}

static void handleTempResult(uint16_t value)
{
	int idx = getTempLookupIdx(value);
	int temp;
	if(idx == -1) { // Temperature below -50
		temp = -500;
	} else if(idx == (sizeof(lmt01LookupTable)/sizeof(uint16_t)-1)) {
		temp = 1500;
	} else {
		int loVal = getTempLookupTemp(idx);
		int loPulseCount = lmt01LookupTable[idx];
		int hiPulseCount = lmt01LookupTable[idx+1];
		int pulseRange = hiPulseCount-loPulseCount;
		temp = loVal + ((value - loPulseCount) * 100) / pulseRange;
		
		int tempOffset = getFanSpeed() > 0 ? 450 : 500;
		int fanspeed = ((temp - tempOffset) * 4)/10;
		setFanSpeed(fanspeed);
		
		if(temp > 900) {
			EmergencyShutdown();
		}
	}
	g_temperature = temp;
}

static void handleVoltageMeasurement(int16_t cha, int16_t chb)
{
	// val * 2.048 * (499+6810) * 1000 / (499*0x8000)
	// ((val * 2.048*(499+6.81e3)*1000)/512)/(499*0x8000/512)
	g_instVoltage[0] = ((29236 * cha)/(31936));
	g_instVoltage[1] = ((29236 * chb)/(31936));
	for (int i =0; i<2; i++)
	{
		g_dispVoltage[i] = (g_dispVoltage[i] + g_instVoltage[i])/2;
		g_decayVoltage[i] = (255 * g_decayVoltage[i] + g_instVoltage[i])/256;
		if(g_instVoltage[i] > g_decayVoltage[i]) g_decayVoltage[i] = g_instVoltage[i];
		setPowerLimitVoltage(i, g_decayVoltage[i]/10+400);
	}
	static int j = 0;
	if(j == 10)
	{
		//logMsg("V: %4d, %4d\n", g_instVoltage[0], g_decayVoltage[0]);
		j = 0;
	}else {
		j++;
	}
}

static void handleCurrentMeasurement(uint16_t cha, uint16_t chb)
{
	// val*2.048*200/(0.05*5600*0xFFFF)
	// (val*(2.048*200*1000/8))/(0.05*5600*0x8000/8)

	g_dispCurrent[0] = cha*51200/(1146880);
	g_dispCurrent[1] = chb*51200/(1146880);
}

static void handleIDriveMeasurement(uint16_t cha, uint16_t chb)
{
	int limit[2];
	for(int i = 0; i < 2; i++) {
		limit[i] = ((getChannelPreVoltage(i)-150)*253952)/27187;
	}
	g_cc[0] = cha < limit[0];
	g_cc[1] = chb < limit[1];
}

void sensingTask(void *parameters)
{
	(void)parameters;
	portDBG_TRACE("Task started");
	
	initADC(0);
	initADC(1);
	initTempSensor();
	
	volatile avr32_tc_channel_t *tempTimer = &AVR32_TC.channel[TEMP_TIMER];
	TickType_t xLastWakeTime = xTaskGetTickCount();
	uint16_t lastTempValue = 0;
	uint16_t measA, measB;
	int lastMeasType = 0;
	for(;;)
	{
		measA = ADC_getResult(0);
		measB = ADC_getResult(1);
		switch(lastMeasType) {
		case 0:
		default:
			ADC_startConversion(0, ADS1115_MEAS_I);
			ADC_startConversion(1, ADS1115_MEAS_I);
			lastMeasType = 1;
			handleVoltageMeasurement(measA, measB);
			break;
		case 1:
			ADC_startConversion(0, ADS1115_MEAS_DRVI);
			ADC_startConversion(1, ADS1115_MEAS_DRVI);
			lastMeasType = 2;
			handleCurrentMeasurement(measA, measB);
			break;
		case 2:
			ADC_startConversion(0, ADS1115_MEAS_V);
			ADC_startConversion(1, ADS1115_MEAS_V);
			lastMeasType = 0;
			handleIDriveMeasurement(measA, measB);
			break;
		}

		uint16_t currentTempValue = tempTimer->cv;
		if(currentTempValue == lastTempValue && currentTempValue != 0) {
			tempTimer->ccr = 0x4; // Reset Timer
			handleTempResult(currentTempValue);
		}
		lastTempValue = currentTempValue;
		vTaskDelayUntil(&xLastWakeTime, TASK_PERIOD_SENSING * portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}